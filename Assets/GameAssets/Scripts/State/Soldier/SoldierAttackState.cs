﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;

namespace GameAssets.Scripts.State.Soldier
{
    public class SoldierAttackState : BaseState
    {
        private readonly GameBase.GamePlay.Monster.Monster _target;

        public SoldierAttackState(GameBase.GamePlay.Monster.Monster target)
        {
            _target = target;
        }

        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.ChangeAnimationState(2);
            Character.stat.ChangeCharacterState(CharacterState.Attack);

            Character.agent.StopAgent();
            Character.ResetVelocity();
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            SetIsComplete(true);
            /*_target.ChangeNewTarget(Character);*/
            Character.animator.OnComplete(OnCompleted);
        }

        protected override void InnerOnComplete()
        {
            base.InnerOnComplete();
            Character.agent.StopAgent();
            Character.ResetVelocity();
            Character.AttackTarget(_target);
        }
    }
}