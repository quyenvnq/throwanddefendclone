﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay.Soldier;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;

namespace GameAssets.Scripts.State.Soldier
{
    public class SoldierMoveState : BaseState
    {
        private readonly GameBase.GamePlay.Monster.Monster _target;

        public SoldierMoveState(GameBase.GamePlay.Monster.Monster target)
        {
            _target = target;
        }

        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.ChangeAnimationState(1);
            (Character as HorseSoldier)?.Move();
            Character.stat.ChangeCharacterState(CharacterState.Move);

            Character.agent.StopAgent();
            Character.ResetVelocity();
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            if (_target == null || !_target.stat.IsAlive)
            {
                ((GameBase.GamePlay.Soldier.Soldier) Character).ChangeNewTarget();
                return;
            }

            if (Character == null)
            {
                return;
            }

            var t = _target.transform;

            Character.transform.LookAt(t);
            Character.agent.SetDestination(t.position);

            if (!Character.Distance(_target, Character.stat.rangeAtk))
            {
                return;
            }

            ((GameBase.GamePlay.Soldier.Soldier) Character).ChangeAttack();
        }
    }
}