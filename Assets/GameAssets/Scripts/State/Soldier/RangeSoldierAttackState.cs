﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;

namespace GameAssets.Scripts.State.Soldier
{
    public class RangeSoldierAttackState : BaseState
    {
        private readonly GameBase.GamePlay.Monster.Monster _target;

        public RangeSoldierAttackState(GameBase.GamePlay.Monster.Monster target)
        {
            _target = target;
        }

        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.ChangeAnimationState(2);
            Character.stat.ChangeCharacterState(CharacterState.Attack);

            Character.agent.StopAgent();
            Character.ResetVelocity();
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            SetIsFrameCompleted(true);
            SetIsComplete(true);

            /*_target.ChangeNewTarget(Character);*/
            Character.animator.OnComplete(OnCompleted, Character.stat.frameAtk, OnFrameCompleted);
        }

        protected override void InnerOnComplete()
        {
            base.InnerOnComplete();
            Character.ResetVelocity();
            ((GameBase.GamePlay.Soldier.Soldier) Character).DestroyMonster(_target);
            Character.animator.ResetNormalizedTime("Slash");
        }

        protected override void InnerOnFrameCompleted()
        {
            base.InnerOnFrameCompleted();
            Character.agent.StopAgent();
            Character.ResetVelocity();
            Character.AttackTarget(_target);
        }
    }
}