﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay.Soldier;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;

namespace GameAssets.Scripts.State.Soldier
{
    public class HorseSoldierAttackState : BaseState
    {
        private readonly GameBase.GamePlay.Monster.Monster _target;

        public HorseSoldierAttackState(GameBase.GamePlay.Monster.Monster target)
        {
            _target = target;
        }

        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.ChangeAnimationState(2);
            Character.stat.ChangeCharacterState(CharacterState.Attack);

            Character.agent.StopAgent();
            Character.ResetVelocity();
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            SetIsComplete(true);
            /*_target.ChangeNewTarget(Character);*/
            ((HorseSoldier) Character).AnimatorHorseman.OnComplete(OnCompleted);
        }

        protected override void InnerOnComplete()
        {
            base.InnerOnComplete();
            Character.agent.StopAgent();
            Character.ResetVelocity();
            Character.AttackTarget(_target);
        }
    }
}