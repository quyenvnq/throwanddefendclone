﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay.Monster;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

namespace GameAssets.Scripts.State.Monster
{
    public class MonsterMoveState : BaseState
    {
        private readonly Transform _target;
        private Vector3 _offset;

        public MonsterMoveState(Transform target)
        {
            _target = target;
        }

        protected override void InnerStartState()
        {
            base.InnerStartState();
            (Character as HorseMonster)?.Move();
            Character.ChangeAnimationState(1);
            Character.stat.ChangeCharacterState(CharacterState.Move);

            Character.agent.StopAgent();
            Character.ResetVelocity();
            _offset = new Vector3(Random.Range(-8f, 8f), 0f, 0f);

            if (!Character.stat.IsAlive)
            {
                return;
            }

            Character.transform.LookAt(_target);

            if (_target == null || Character.agent == null)
            {
                return;
            }

            Character.agent.SetDestination(_target.position + _offset);
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            if (_target == null)
            {
                Character.ChangeState();
                ((GameBase.GamePlay.Monster.Monster) Character).ChangeWallTarget();
                return;
            }

            if (!Character.stat.IsAlive || Vector3.Distance(Character.transform.position, _target.position + _offset) >
                Character.stat.rangeAtk + 6f)
            {
                return;
            }

            ((GameBase.GamePlay.Monster.Monster) Character).ChangeAttackDistance();
        }
    }
}