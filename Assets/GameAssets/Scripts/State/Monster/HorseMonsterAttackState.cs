﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay.Monster;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;

namespace GameAssets.Scripts.State.Monster
{
    public class HorseMonsterAttackState : BaseState
    {
        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.ChangeAnimationState(2);
            Character.stat.ChangeCharacterState(CharacterState.Attack);
            
            Character.agent.StopAgent();
            Character.ResetVelocity();
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            SetIsComplete(true);
            ((HorseMonster) Character).AnimatorHorseman.OnComplete(OnCompleted);
        }

        protected override void InnerOnComplete()
        {
            base.InnerOnComplete();
            Character.AttackTarget();
        }
    }
}