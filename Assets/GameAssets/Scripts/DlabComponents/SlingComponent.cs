using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.DlabComponents
{
    public class SlingComponent : MonoBehaviour
    {
        [SerializeField] AnimationCurve m_slingLine;
        [SerializeField] private SkinnedMeshRenderer m_skinnedMesh;

        [Button]
        public void DoShoot()
        {
            float bouceValue = m_skinnedMesh.GetBlendShapeWeight(1);
            Debug.Log(bouceValue);
            DOTween.To(() => bouceValue, x => bouceValue = x, 0, 0.3f).SetEase(m_slingLine).OnUpdate(()=> { m_skinnedMesh.SetBlendShapeWeight(1, bouceValue); });
        }
        public void DoDrag(int value)
        {
            m_skinnedMesh.SetBlendShapeWeight(1, value);
        }
        Vector2 m_origin, m_end;
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_origin = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                m_end = Input.mousePosition;
                int dragValue = Mathf.Abs(System.Convert.ToInt32( m_end.y) - System.Convert.ToInt32(m_origin.y));
                DoDrag(System.Convert.ToInt32(dragValue));
                Debug.Log(System.Convert.ToInt32(dragValue / Camera.main.pixelHeight));
            }
            else if (Input.GetMouseButtonUp(0))
            {
                DoShoot();
                m_origin = m_end = Vector2.zero;
            }
        }
    }
}
