﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.DlabComponents
{
    public class FieldOfView : MonoBehaviour
    {
        public float m_viewRadius;
        [Range(0, 360)]
        public float m_viewAngle;
        //public SpriteRenderer m_viewOnScene;
        public List<Transform> m_visibleTargets = new List<Transform>();
        void Start()
        {
            StartCoroutine("FindTargetsWithDelay", 0f);
        }

        IEnumerator FindTargetsWithDelay(float delay)
        {
            while (true)
            {
                yield return new WaitForSeconds(delay);
                FindVisibleTargets();
            }
        }
        private Vector3 m_dirToTarget;
        void FindVisibleTargets()
        {
            m_visibleTargets.Clear();
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, m_viewRadius);
            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                Transform target = targetsInViewRadius[i].transform;
                m_dirToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, m_dirToTarget) < m_viewAngle / 2)
                {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);
                    if (!Physics.Raycast(transform.position, m_dirToTarget, dstToTarget, 2))
                    {
                        m_visibleTargets.Add(target);
                    }
                }
            }
        }

        public List<T> FindWithType<T>()
        {
            List<T> visibles = new List<T>();
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, m_viewRadius);
            for (int i = 0; i < targetsInViewRadius.Length; i++)
            {
                Transform target = targetsInViewRadius[i].transform;
                m_dirToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, m_dirToTarget) < m_viewAngle / 2)
                {
                    float dstToTarget = Vector3.Distance(transform.position, target.position);
                    if (!Physics.Raycast(transform.position, m_dirToTarget, dstToTarget, 2))
                    {
                        if(target.GetComponent<T>() != null)
                        {
                            visibles.Add(target.GetComponent<T>());
                        }
                    }
                }
            }
            return visibles;
        }

        private void OnDrawGizmos()
        {
#if UNITY_EDITOR
            Handles.color = Color.yellow;
            Handles.DrawWireDisc(this.transform.position, Vector3.up, m_viewRadius);
            Handles.color = Color.red;
            Handles.DrawLine(this.transform.position, this.transform.position + DirFromAngle(m_viewAngle, false) * m_viewRadius);
            Handles.DrawLine(this.transform.position, this.transform.position + DirFromAngle(-m_viewAngle, false) * m_viewRadius);
            //m_viewOnScene.transform.localScale = new Vector3(m_viewRadius, m_viewRadius, m_viewRadius);
#endif
        }

        public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
        {
            if (!angleIsGlobal)
            {
                angleInDegrees += transform.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }
    }
}
