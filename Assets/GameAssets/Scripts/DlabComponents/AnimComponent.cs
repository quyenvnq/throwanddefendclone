using System;
using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.DlabComponents
{
    [Serializable]
    public class BoneConfig
    {
        public Vector3 m_pos;
        public Vector3 m_rot;

        public BoneConfig(Vector3 pos, Vector3 rot)
        {
            m_pos = pos;
            m_rot = rot;
        }

        public BoneConfig()
        {
        }
    }

    public class AnimComponent : MonoBehaviour
    {
        [SerializeField] private List<Rigidbody> m_ribs = new List<Rigidbody>();
        [SerializeField] private List<Rigidbody> m_ribs_kinematic = new List<Rigidbody>();

        [SerializeField] private List<Transform> m_bones = new List<Transform>();
        [SerializeField] private List<BoneConfig> m_bonecfg = new List<BoneConfig>();
        [SerializeField] private AnimationCurve m_jump;

        [SerializeField] private float delayTimeAfterKnockback = 3f;
        [SerializeField] private float jumpDuration = 3f;

        [Button]
        public void Init()
        {
            m_ribs.Clear();
            m_bones.Clear();
            m_bonecfg.Clear();
            var mTransforms = GetComponentsInChildren<Transform>();

            foreach (var x in mTransforms)
            {
                if (!x.GetComponent<Rigidbody>() || x == transform)
                {
                    continue;
                }

                m_ribs.Add(x.GetComponent<Rigidbody>());
                m_bones.Add(x);
                m_bonecfg.Add(new BoneConfig(x.localPosition, x.localEulerAngles));
            }

            /*OnSwitchRagDoll(false);*/
        }

        [Button]
        public void OnSwitchRagDoll(bool active)
        {
            if (active)
            {
                /*foreach (var x in animators)
                {
                    x.enabled = false;
                }*/
                foreach (var x in m_ribs)
                {
                    x.isKinematic = false;
                    x.ResetInertia();
                }
            }
            else
            {
                /*foreach (var x in m_ribs)
                {
                    x.isKinematic = true;
                    x.ResetInertia();
                }*/
                /*foreach (var x in animators)
                {
                    x.enabled = true;
                }*/
            }
        }

        private void SetKinematic(bool active)
        {
            /*foreach (var x in m_ribs_kinematic)
            {
                x.isKinematic = active;
            }*/
        }

        public void DoJump(Vector3 endValue, float jumpPower, Action onStandUp = null, bool isEnabledAnimator = false,
            float time = 0.25f)
        {
            var duration = Vector3.Distance(transform.position, endValue) / 30 * jumpPower;
            OnSwitchRagDoll(true);
            SetKinematic(true);

            transform.DOJump(endValue, jumpPower, 1, jumpDuration).OnComplete(
                () =>
                {
                    if (isEnabledAnimator)
                    {
                        /*DOVirtual.DelayedCall(time, () =>
                        {
                            foreach (var x in animators)
                            {
                                x.enabled = true;
                            }
                        });*/
                    }
                    else
                    {
                        OnSwitchRagDoll(true);
                    }

                    SetKinematic(false);
                    DOVirtual.DelayedCall(delayTimeAfterKnockback - (isEnabledAnimator ? time : 0f), () =>
                    {
                        if (isEnabledAnimator)
                        {
                            foreach (var x in m_ribs)
                            {
                                x.isKinematic = true;
                                x.ResetInertia();
                            }
                        }
                        else
                        {
                            OnSwitchRagDoll(false);
                        }

                        onStandUp?.Invoke();
                    });
                });
        }

        public void DoPath(Vector3[] path, Action onStandUp = null)
        {
            var duration = Vector3.Distance(transform.position, path[path.Length - 1]) / 30;
            OnSwitchRagDoll(true);

            m_ribs[0].isKinematic = true;
            transform.DOPath(path, duration).SetEase(m_jump).OnComplete(() =>
            {
                OnSwitchRagDoll(true);
                m_ribs[0].isKinematic = false;
                DOVirtual.DelayedCall(delayTimeAfterKnockback, () =>
                {
                    OnSwitchRagDoll(false);
                    onStandUp?.Invoke();
                });
            });
        }
    }
}