﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class GameManager : BaseSingleton<GameManager>
    {
        [ReadOnly] [SerializeField] private GameState currentState;
        [ReadOnly] [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();

        public Camera Camera { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 120;
            Camera = FindObjectOfType<Camera>();
        }

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            foreach (var x in character)
            {
                if (!characters.Contains(x))
                {
                    characters.Add(x);
                }
            }
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            foreach (var x in characters)
            {
                x.currentState?.UpdateState();
            }
        }

        public void SetGameState(GameState gameState)
        {
            if (Equals(currentState, gameState))
            {
                return;
            }

            currentState = gameState;
            /*Debug.Log($"GAME STATE ===> {gameState}");*/
        }

        public bool IsGameState(GameState state)
        {
            return currentState == state;
        }

        public void OnWin()
        {
            SetGameState(GameState.Win);
            LevelManager.SetState(true);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameComplete);
            StopBaseCharacter();
            AppMetricaEvent.SendEventFinish("win");
        }

        public void OnLose()
        {
            SetGameState(GameState.Lose);
            LevelManager.SetState(false);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameOver);
            StopBaseCharacter();
            AppMetricaEvent.SendEventFinish("lose");
        }

        public void StopBaseCharacter()
        {
            foreach (var x in characters.Where(x => !(x is Wall) && x != null && x.stat.IsAlive))
            {
                x.ChangeState();
                x.rbs[0].isKinematic = true;
                x.agent.StopAgent();
                x.ChangeAnimationState(0);
            }
        }
    }
}