﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Manager.UI;
using GameAssets.Scripts.GameBase.Serializable;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        [ReadOnly] [SerializeField] private int currentLevel;

        public UserDataSave userDataSave;
        private UIPlay _uiPlay;

        protected override void Awake()
        {
            base.Awake();
            if (GetDataFromSave() == null)
            {
                ResetUserData();
            }

            userDataSave = GetDataFromSave();
        }

        private void Start()
        {
            _uiPlay = UIManager.Instance.GetUI<UIPlay>();
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            if (Time.frameCount % Const.MIN_INTERVAL != 0)
            {
                return;
            }

            if (userDataSave.seconds >= 59)
            {
                userDataSave.minutes++;
                userDataSave.seconds = 0;
            }

            if (userDataSave.minutes >= 59)
            {
                userDataSave.hours++;
                userDataSave.minutes = 0;
            }

            userDataSave.seconds++;
        }

        private static UserDataSave GetDataFromSave()
        {
            var json = Decrypt(PlayerPrefs.GetString(Const.SAVE_DATA));
            if (json.Length > 0)
            {
                Debug.Log($"GAME DATA ===> {json}");
            }

            return JsonUtility.FromJson<UserDataSave>(json);
        }
        
        private static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var keyBytes =
                new Rfc2898DeriveBytes(Const.PASSWORD_HASH, Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.Zeros
            };

            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }

                memoryStream.Close();
            }

            return Convert.ToBase64String(cipherTextBytes);
        }

        private static string Decrypt(string encryptedText)
        {
            var cipherTextBytes = Convert.FromBase64String(encryptedText);
            var keyBytes =
                new Rfc2898DeriveBytes(Const.PASSWORD_HASH, Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.None
            };

            var decrypt = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            var memoryStream = new MemoryStream(cipherTextBytes);

            var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        private void Save()
        {
            currentLevel = userDataSave.level;
            var json = JsonUtility.ToJson(userDataSave);

            Debug.Log($"GAME DATA SAVED ===> {json}");
            PlayerPrefs.SetString(Const.SAVE_DATA, Encrypt(json));
            PlayerPrefs.Save();
        }

        [Button("Clear User Data")]
        private void ClearUserData()
        {
            PlayerPrefs.DeleteAll();
        }

        [Button("Reset User Data")]
        private void ResetUserData()
        {
            userDataSave = new UserDataSave
            {
                level = 0,
                bestLevel = 0,
                bestScore = 0,
                bestScoreEndless = 0,
                diamond = 0,
                money = 1000,
                death = 0,
                hours = 0f,
                minutes = 0f,
                seconds = 0f,
                sound = true,
                music = true,
                vibrate = true,
                removedAds = false,
                lastOpenDay = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                highscores = new List<Highscore>(),
                upgrades = new List<Upgrade>()
            };

            Save();
        }

        public void SetMusic(bool enable)
        {
            userDataSave.music = enable;
            Save();
        }

        public void SetSound(bool enable)
        {
            userDataSave.sound = enable;
            Save();
        }

        public void SetHaptic(bool enable)
        {
            userDataSave.vibrate = enable;
            Save();
        }

        public void SetBestScoreLevel(int score)
        {
            if (score > userDataSave.bestScore)
            {
                userDataSave.bestScore = score;
            }

            Save();
        }

        public void SetBestScoreEndless(int score)
        {
            if (score > userDataSave.bestScoreEndless)
            {
                userDataSave.bestScoreEndless = score;
            }

            Save();
        }

        public void LevelUp()
        {
            userDataSave.level++;
            Save();
        }

        public void AddDeath()
        {
            userDataSave.death++;
            Save();
        }

        public bool CheckMoney(float price)
        {
            return userDataSave.money >= price;
        }

        public void AddMoney(float value, bool isSave = true)
        {
            userDataSave.money += value;
            if (isSave)
            {
                Save();
            }
        }

        public void AddDiamond(float value, bool isSave = true)
        {
            userDataSave.diamond += value;
            if (isSave)
            {
                Save();
            }
        }

        public void AddHighscore(params Highscore[] hs)
        {
            userDataSave.highscores.AddRange(hs);
            Save();
        }

        public void AddUpgrade(params Upgrade[] u)
        {
            userDataSave.upgrades.AddRange(u);
            Save();
        }

        public void SetUpgrade(params Upgrade[] u)
        {
            userDataSave.upgrades.Clear();
            AddUpgrade(u);
        }

        public void SetBestLevel(int level)
        {
            if (level > userDataSave.bestLevel)
            {
                userDataSave.bestLevel = level;
            }

            Save();
        }

        public void SetRemovedAds()
        {
            userDataSave.removedAds = true;
            Save();
        }
    }
}