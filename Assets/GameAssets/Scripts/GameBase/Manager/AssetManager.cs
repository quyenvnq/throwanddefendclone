﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class AssetManager : BaseSingleton<AssetManager>
    {
        public Sprite uiSoundOn;
        public Sprite uiSoundOff;
        public Sprite uiMusicOn;
        public Sprite uiMusicOff;
        public Sprite uiHapticOn;
        public Sprite uiHapticOff;
        public Sprite uiUseWatchAds;
        public Sprite uiUseByMoney;
        public Sprite uiUseByDiamond;
        public Sprite uiBtnBuyColor;
        public Sprite uiBtnBought;

        public void SetSpriteMusic(Image imgMusic, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetMusic(!UserDataManager.Instance.userDataSave.music);
            }

            imgMusic.sprite = UserDataManager.Instance.userDataSave.music ? uiMusicOn : uiMusicOff;
        }

        public void SetSpriteSound(Image imgSound, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetSound(!UserDataManager.Instance.userDataSave.sound);
            }

            imgSound.sprite = UserDataManager.Instance.userDataSave.sound ? uiSoundOn : uiSoundOff;
        }

        public void SetSpriteHaptic(Image imgHaptic, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetHaptic(!UserDataManager.Instance.userDataSave.vibrate);
            }

            imgHaptic.sprite = UserDataManager.Instance.userDataSave.vibrate ? uiHapticOn : uiHapticOff;
        }
    }
}