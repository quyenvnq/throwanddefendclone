﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private List<GameObject> levels = new List<GameObject>();
        [SerializeField] private Transform parent;

        [SerializeField] private int amountChangeLevel = 2;
        [SerializeField] private bool isLoopLevel;

        [ReadOnly] public int currentLevel;

        private static bool? IsChangeBg { get; set; } = false;
        private static int _indexBg;
        private static GameObject _currentLevel;

        protected override void Awake()
        {
            base.Awake();
            currentLevel = UserDataManager.Instance.userDataSave.level;
            LoadLevel();
        }

        public static void SetState(bool? active)
        {
            IsChangeBg = active;
        }

        private void LoadLevel()
        {
            if (IsChangeBg == null)
            {
                SpawnerHelper.CreateSpawner(Vector3.zero, parent, _currentLevel);
                return;
            }

            if (levels.Count <= 0)
            {
                return;
            }

            if (_currentLevel == null)
            {
                _currentLevel = levels[0];
            }
            else
            {
                if (IsChangeBg == true && currentLevel % amountChangeLevel == 0)
                {
                    _indexBg++;
                    if (_indexBg > levels.Count - 1)
                    {
                        _indexBg = isLoopLevel ? 0 : levels.Count - 1;
                    }

                    _currentLevel = levels[_indexBg];
                }
            }

            SpawnerHelper.CreateSpawner(Vector3.zero, parent, _currentLevel);
        }
    }
}