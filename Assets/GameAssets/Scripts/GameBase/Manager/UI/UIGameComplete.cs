﻿using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameComplete : BaseUI
    {
        [SerializeField] private Button btnNextLevel;
        [SerializeField] private Image imgHealthFilled;

        [SerializeField] private TextMeshProUGUI txtCurrentMoney;
        [SerializeField] private TextMeshProUGUI txtWinMoney;

        [SerializeField] private GameObject coinPrefab;
        [SerializeField] private Ease ease;
        [SerializeField] private Vector3 scale = Vector3.zero;

        [Range(0.5f, 0.9f)] [SerializeField] private float minAnimationDuration;
        [Range(0.9f, 2f)] [SerializeField] private float maxAnimationDuration;
        [SerializeField] private float radius = 100f;
        [SerializeField] private int amountCoin = 20;

        private readonly Queue<RectTransform> _coinQueue = new Queue<RectTransform>();

        private int _money;
        private bool _isComplete;

        protected override void Start()
        {
            base.Start();
            btnNextLevel.onClick.AddListener(() =>
            {
                if (_isComplete)
                {
                    return;
                }

                _isComplete = true;

                DoEffect(() => SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name));
                UserDataManager.Instance.LevelUp();
            });
        }

        private void OnEnable()
        {
            _money = WaveConfig.Instance.MonsterCoinWin;
            imgHealthFilled.fillAmount = Wall.Instance.CurrentFillAmount();

            txtWinMoney.text = $"{_money}";
            txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
            _coinQueue.Clear();
        }

        private void DoEffect(System.Action action)
        {
            CreateCoins(amountCoin);

            for (var i = 0; i < _money; i++)
            {
                if (_coinQueue.Count <= 0)
                {
                    CreateCoins(_money >= amountCoin ? amountCoin : amountCoin - _money);
                }

                var coin = _coinQueue.Dequeue();
                var duration = Random.Range(minAnimationDuration, maxAnimationDuration);

                coin.gameObject.SetActive(true);
                coin.transform.DOScale(Vector3.one, 0.5f).OnComplete(() =>
                {
                    coin.transform.DOScale(scale, duration);
                });

                coin.transform
                    .DOMove(coinPrefab.transform.position + (Vector3) (Vector3.one * Random.insideUnitCircle * radius),
                        0.5f).OnComplete(
                        () =>
                        {
                            coin.transform.DOMove(txtCurrentMoney.transform.position, duration)
                                .SetEase(ease)
                                .OnComplete(
                                    () =>
                                    {
                                        coin.gameObject.SetActive(false);
                                        _coinQueue.Enqueue(coin);

                                        _money--;
                                        UserDataManager.Instance.AddMoney(1, _money == 0);

                                        txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
                                        txtWinMoney.text = $"{_money}";
                                    });
                        });
            }

            DOVirtual.DelayedCall(2.5f, () => action());
        }

        private void CreateCoins(int amount)
        {
            for (var i = 0; i < amount; i++)
            {
                var coin = Instantiate(coinPrefab).GetComponent<RectTransform>();
                coin.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

                coin.SetParent(coinPrefab.transform.parent, false);
                coin.transform.position = coinPrefab.transform.position;

                coin.gameObject.SetActive(false);
                _coinQueue.Enqueue(coin);
            }
        }
    }
}