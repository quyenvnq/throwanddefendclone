﻿using GameAssets.Scripts.GameBase.Base;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameOver : BaseUI
    {
        [SerializeField] private Button btnReplay;
        [SerializeField] private TextMeshProUGUI txtCurrentMoney;

        protected override void Start()
        {
            base.Start();
            btnReplay.onClick.AddListener(() => { SceneManager.LoadScene(SceneManager.GetActiveScene().name); });
            txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
        }
    }
}