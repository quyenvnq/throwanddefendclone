﻿using System.Collections.Generic;
using System.Linq;
using Demigiant.DOTween.Modules;
using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Serializable;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameStart : BaseUI
    {
        public List<Weapon> weapons = new List<Weapon>();
        [SerializeField] private TextMeshProUGUI txtCurrentMoney;
        [SerializeField] private Transform weaponParent;
        [SerializeField] private Button btnRestart;
        [SerializeField] private Image imgHealthFilled;

        [SerializeField] private float lerpSpeed;

        private Weapon _currentWeapon;

        protected override void Start()
        {
            base.Start();
            txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
            
            btnRestart.onClick.AddListener(() =>
            {
                LevelManager.SetState(null);
                SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
            });

            weapons.AddRange(LevelConfig.Instance.GetSubWeapons());
            
            foreach (var x in weapons)
            {
                x.btnWeapon.onClick.AddListener(() => ClickChangeWeapon(x));
            }
            
            AppMetricaEvent.SendEventStart();
        }

        public void ChangeWeapon()
        {
            SetActiveWeapon(false);
            _currentWeapon = RandomWeapon();

            _currentWeapon.btnWeapon.gameObject.SetActive(true);
            _currentWeapon.btnWeapon.transform.SetAsLastSibling();

            SetActiveWeapon(true);
            var children = weaponParent.childCount;
            for (var i = 0; i < children; i++)
            {
                weaponParent.GetChild(i).gameObject.SetActive(i >= children - 3);
            }
        }

        private void ClickChangeWeapon(Weapon w)
        {
            SetActiveWeapon(false);
            _currentWeapon = weapons.Find(x => x.Equals(w));
            
            _currentWeapon.btnWeapon.gameObject.SetActive(true);
            _currentWeapon.btnWeapon.transform.SetAsLastSibling();

            SetActiveWeapon(true);
            var children = weaponParent.childCount;
            for (var i = 0; i < children; i++)
            {
                weaponParent.GetChild(i).gameObject.SetActive(i >= children - 3);
            }
        }

        private void SetActiveWeapon(bool isActive)
        {
            if (_currentWeapon == null || _currentWeapon.prefab == null)
            {
                return;
            }

            _currentWeapon.imgProgressBar.gameObject.SetActive(isActive);
            _currentWeapon.imgProgressBar.fillAmount = 0;

            if (isActive)
            {
                _currentWeapon.imgProgressBar.DOFillAmount(1f, lerpSpeed).OnComplete(
                    () => Player.Instance.CreateBullet(_currentWeapon));
            }
        }

        private Weapon RandomWeapon()
        {
            var range = weapons.Sum(x => x.percentDrop);
            var rnd = Random.Range(0, range);
            var top = 0f;

            foreach (var x in weapons)
            {
                top += x.percentDrop;
                if (rnd < top)
                {
                    return x;
                }
            }

            return null;
        }

        public bool CanShootSoldier()
        {
            return _currentWeapon.imgProgressBar.fillAmount >= 0.99f;
        }

        public void UpdateHealthBar(float target)
        {
            imgHealthFilled.DOFillAmount(target, lerpSpeed);
        }
    }
}