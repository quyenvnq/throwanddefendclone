﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Serializable;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIPlay : BaseUI
    {
        public List<Upgrade> upgrades = new List<Upgrade>();
        [SerializeField] private Button btnTapToStart;
        [SerializeField] private Button btnSound;
        [SerializeField] private Button btnHaptic;
        [SerializeField] private TextMeshProUGUI txtCurrentMoney;

        private UIGameStart _uiGameStart;

        protected override void Start()
        {
            base.Start();
            txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
            _uiGameStart = UIManager.Instance.GetUI<UIGameStart>();

            for (var i = 0; i < upgrades.Count; i++)
            {
                var item = upgrades[i];
                if (UserDataManager.Instance.userDataSave.upgrades.Count > 0)
                {
                    item.SetUpgrade(UserDataManager.Instance.userDataSave.upgrades[i]);
                }

                item.btnUpgrade.onClick.AddListener(() =>
                {
                    if (!item.LevelUp())
                    {
                        return;
                    }

                    UserDataManager.Instance.SetUpgrade(upgrades.ToArray());
                    txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
                });
            }

            btnTapToStart.onClick.AddListener(() =>
            {
                UIManager.Instance.HideAllAndShowUI(UIType.UIGameStart);
                _uiGameStart.ChangeWeapon();
                GameManager.Instance.SetGameState(GameState.Playing);
            });

            btnSound.onClick.AddListener(() =>
            {
                AssetManager.Instance.SetSpriteSound(btnSound.GetComponent<Image>());
            });

            btnHaptic.onClick.AddListener(() =>
            {
                AssetManager.Instance.SetSpriteHaptic(btnHaptic.GetComponent<Image>());
            });

            AssetManager.Instance.SetSpriteSound(btnSound.GetComponent<Image>(), true);
            AssetManager.Instance.SetSpriteHaptic(btnHaptic.GetComponent<Image>(), true);
        }
    }
}