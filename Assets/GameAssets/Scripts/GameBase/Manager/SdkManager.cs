using AppsFlyerSDK;
using Facebook.Unity;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class SdkManager : BaseSingleton<SdkManager>
    {
        [Header("Initiation")] [SerializeField]
        private bool initFbSdk;

        [SerializeField] private bool initAppFlyer;

        [Header("App id")] [SerializeField] private string idAppFlyer;
        [SerializeField] private string idDevKeyAppFlyer;

        protected override void InitAwake()
        {
            base.InitAwake();

            try
            {
                if (initFbSdk)
                {
                    FB.Init(FbInitComplete);
                    Debug.Log($"FB Called with {FB.AppId}");
                }

                if (!initAppFlyer)
                {
                    return;
                }


                AppsFlyer.initSDK(idDevKeyAppFlyer, idAppFlyer);

                if (Application.platform.Equals(RuntimePlatform.IPhonePlayer))
                {
                    AppsFlyer.getConversionData(name);
                }
            }
            catch
            {
                // ignored
            }
        }

        private static void FbInitComplete()
        {
            Debug.Log("FB init complete");
        }
    }
}