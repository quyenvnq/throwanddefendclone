﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    internal static class DestructionHelper
    {
        public static void Toggle(ref bool value, string name)
        {
            GUILayout.BeginHorizontal("box");
            GUILayout.Label(name + " : ");
            value = GUILayout.Toggle(value, "");
            GUILayout.EndHorizontal();
        }

        public static T[] FindObjectsOfType<T>() => Object.FindObjectsOfType(typeof(T)) as T[];

        public static T[] GetInterfaces<T>(this GameObject go)
        {
            if (!typeof(T).IsInterface)
            {
                throw new SystemException("Specified type is not an interface!");
            }

            return go.GetComponents<T>()
                .Where(a => a.GetType().GetInterfaces().Any(k => k == typeof(T))).Select(a => a).ToArray();
        }

        public static List<Vector3> DedupeCollectionWithRandom(this IEnumerable<Vector3> input)
        {
            var hs = new HashSet<Vector3>();
            foreach (var x in input.Where(y => !hs.Contains(y)))
            {
                hs.Add(x + Random.insideUnitSphere * 0.0001f);
            }

            return new List<Vector3>(hs);
        }

        public static List<Vector3> DedupeCollectionWithRandom(this IEnumerable<Vector3> input,
            IEqualityComparer<Vector3> comparer)
        {
            var hs = new HashSet<Vector3>(comparer);
            foreach (var x in input)
            {
                if (!hs.Contains(x))
                {
                    hs.Add(x + Random.insideUnitSphere * 0.0001f);
                }
            }

            return new List<Vector3>(hs);
        }

        public static List<T> DedupeCollection<T>(this IEnumerable<T> input)
        {
            var hs = new HashSet<T>();
            foreach (var x in input)
            {
                if (!hs.Contains(x))
                {
                    hs.Add(x);
                }
            }

            return new List<T>(hs);
        }
    }
}