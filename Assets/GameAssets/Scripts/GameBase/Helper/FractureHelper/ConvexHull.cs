﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    public static class ConvexHull
    {
        private static List<ShardFace> _validFaces;
        private static List<ShardFace> _visibleFaces;
        private static List<ShardFace> _tmpFaces;

        public static int[] ComputeIncremental(this Vector3[] points)
        {
            if (points.Length < 3)
            {
                return null;
            }

            ShardFace.Points = points;

            var face1 = new ShardFace(0, 1, 2);
            var p = Centroid(points, 3, face1);
            if (face1.IsVisible(p))
            {
                face1.Flip();
            }

            var face2 = new ShardFace(3, face1.I0, face1.I1);
            if (face2.IsVisible(p))
            {
                face2.Flip();
            }

            var face3 = new ShardFace(3, face1.I1, face1.i2);
            if (face3.IsVisible(p))
            {
                face3.Flip();
            }

            var face4 = new ShardFace(3, face1.i2, face1.I0);
            if (face4.IsVisible(p))
            {
                face4.Flip();
            }

            _validFaces = new List<ShardFace>
            {
                face1,
                face2,
                face3,
                face4
            };

            _visibleFaces = new List<ShardFace>(points.Length);
            _tmpFaces = new List<ShardFace>(points.Length);

            for (var i = 4; i < points.Length; ++i)
            {
                var point = points[i];
                _visibleFaces.Clear();

                foreach (var t in _validFaces.Where(t => t.IsVisible(point)))
                {
                    _visibleFaces.Add(t);
                }

                if (_visibleFaces.Count.Equals(0))
                {
                    continue;
                }

                foreach (var t in _visibleFaces)
                {
                    _validFaces.Remove(t);
                }

                if (_visibleFaces.Count.Equals(1))
                {
                    var visibleShardFace = _visibleFaces[0];
                    _validFaces.Add(new ShardFace(i, visibleShardFace.I0, visibleShardFace.I1));
                    _validFaces.Add(new ShardFace(i, visibleShardFace.I1, visibleShardFace.i2));
                    _validFaces.Add(new ShardFace(i, visibleShardFace.i2, visibleShardFace.I0));
                }
                else
                {
                    if (_visibleFaces.Count > 2000)
                    {
                        Debug.LogWarning("Visible faces is too big, " + _visibleFaces.Count +
                                         " cancelling operation.");
                        return new int[0];
                    }

                    _tmpFaces.Clear();
                    foreach (var t in _visibleFaces)
                    {
                        _tmpFaces.Add(new ShardFace(i, t.I0, t.I1));
                        _tmpFaces.Add(new ShardFace(i, t.I1, t.i2));
                        _tmpFaces.Add(new ShardFace(i, t.i2, t.I0));
                    }

                    if (_tmpFaces.Count > 8000)
                    {
                        Debug.LogWarning("Temp faces is too big, " + _tmpFaces.Count + " cancelling operation.");
                        return new int[0];
                    }

                    foreach (var t1 in _tmpFaces)
                    {
                        var face5 = t1;
                        if (_tmpFaces.Any(t => face5 != t && face5.IsVisible(t.Centroid)))
                        {
                            face5 = null;
                        }

                        if (face5 != null)
                        {
                            _validFaces.Add(face5);
                        }
                    }
                }
            }

            var numArray1 = new int[_validFaces.Count * 3];
            var num1 = 0;

            foreach (var t in _validFaces)
            {
                var index2 = num1;
                var num2 = index2 + 1;
                var i0 = t.I0;
                numArray1[index2] = i0;

                var num3 = num2 + 1;
                var i1 = t.I1;

                numArray1[num2] = i1;
                num1 = num3 + 1;

                var i2 = t.i2;
                numArray1[num3] = i2;
            }

            return numArray1;
        }

        private static Vector3 Centroid(this IReadOnlyList<Vector3> points, int index, ShardFace face) =>
            (points[index] + points[face.I0] + points[face.I1] + points[face.i2]) / 4f;
    }
}