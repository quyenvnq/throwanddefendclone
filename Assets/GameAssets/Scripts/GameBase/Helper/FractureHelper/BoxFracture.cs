﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    [RequireComponent(typeof(Rigidbody))]
    public sealed class BoxFracture : BaseFracture
    {
        private List<Vector3> _sortedVoronoiPoints;

        protected override void ImmediateFracture(Vector3[] points)
        {
            InitializeDestruction();
            _sortedVoronoiPoints = new List<Vector3>(points);

            foreach (var t in points)
            {
                CreateInitialPlanes(t);
                _sortedVoronoiPoints.Sort((p1, p2) => (p1 - t).sqrMagnitude.CompareTo((p2 - t).sqrMagnitude));

                CalculateVerts(t);
                if (vertices.Count <= 3)
                {
                    continue;
                }

                newVertices = vertices.ToArray();
                newTriangles = newVertices.ComputeIncremental();
                if (newTriangles != null)
                {
                    CreateShardMesh(t);
                }
            }

            FinalizeDestruction();
        }

        protected override async void SpreadFracture(Vector3[] points)
        {
            InitializeDestruction();
            _sortedVoronoiPoints = new List<Vector3>(points);

            var currentShard = 0;
            var remainingShards = points.Length;

            while (remainingShards > 0)
            {
                for (var index = 0; index < Mathf.Min(shardsPerFrame, remainingShards); ++index)
                {
                    --remainingShards;
                    var point = points[currentShard++];

                    CreateInitialPlanes(point);
                    _sortedVoronoiPoints.Sort((p1, p2) =>
                        (p1 - point).sqrMagnitude.CompareTo((p2 - point).sqrMagnitude));

                    CalculateVerts(point);
                    if (vertices.Count <= 3)
                    {
                        continue;
                    }

                    newVertices = vertices.ToArray();
                    newTriangles = newVertices.ComputeIncremental();
                    if (newTriangles != null)
                    {
                        CreateShardMesh(point);
                    }
                }

                await Task.Yield();
            }

            FinalizeDestruction();
        }

        private void CreateInitialPlanes(Vector3 point)
        {
            var v1 = point - maxBounds;
            var v2 = minBounds - point;

            InitPlanes[0].w = v1.x;
            InitPlanes[1].w = v1.y;
            InitPlanes[2].w = v1.z;
            InitPlanes[3].w = v2.x;
            InitPlanes[4].w = v2.y;
            InitPlanes[5].w = v2.z;

            planes.Clear();
            planes.AddRange(InitPlanes);
        }

        private void CalculateVerts(Vector3 point)
        {
            var num = float.PositiveInfinity;
            for (var index = 1; index < _sortedVoronoiPoints.Count; ++index)
            {
                var vector3 = _sortedVoronoiPoints[index] - point;
                var sqrMagnitude = vector3.sqrMagnitude;

                if (sqrMagnitude > num * (double) num)
                {
                    break;
                }

                var normalized = (Vector4) vector3.normalized;

                normalized.w = (float) (-(double) Mathf.Sqrt(sqrMagnitude) / 2.0);
                planes.Add(normalized);
                GetVerticesInPlane();

                if (vertices.Count <= 3)
                {
                    break;
                }

                ReorderPlanes();
                num = GetMaxDistance();
            }
        }

        private float GetMaxDistance()
        {
            var f = vertices[0].sqrMagnitude;
            var count = vertices.Count;
            for (var index = 1; index < count; ++index)
            {
                var sqrMagnitude = vertices[index].sqrMagnitude;
                if (f < (double) sqrMagnitude)
                {
                    f = sqrMagnitude;
                }
            }

            return Mathf.Sqrt(f) * 2f;
        }

        private void ReorderPlanes()
        {
            if (planeIndices.Count.Equals(planes.Count))
            {
                return;
            }

            var i = 0;

            foreach (var planeIndex in planeIndices)
            {
                if (!i.Equals(planeIndex))
                {
                    planes[i] = planes[planeIndex];
                }

                ++i;
            }

            var count = planeIndices.Count - i;
            if (count >= 0)
            {
                return;
            }

            planes.RemoveRange(i - 1, count);
        }
    }
}