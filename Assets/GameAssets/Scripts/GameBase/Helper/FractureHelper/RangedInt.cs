﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    [Serializable]
    public class RangedInt
    {
        [SerializeField] private int min;
        [SerializeField] private int max;

        public RangedInt(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        public int Random() => UnityEngine.Random.Range(min, max);

        public int Clamp(int input) => Mathf.Clamp(input, min, max);
    }
}