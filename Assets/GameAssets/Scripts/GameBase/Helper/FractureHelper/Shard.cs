﻿using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    public class Shard : MonoBehaviour
    {
        private MeshFilter _meshFilter;
        private MeshCollider _meshCollider;

        private Mesh Mesh => _meshFilter.mesh;

        private Renderer _renderer;

        private void Awake()
        {
            _renderer = gameObject.AddComponent<MeshRenderer>();
            gameObject.AddComponent<Rigidbody>();

            _meshFilter = gameObject.AddComponent<MeshFilter>();
            _meshCollider = gameObject.AddComponent<MeshCollider>();

            _meshCollider.convex = true;
            _meshFilter.mesh = new Mesh();
            gameObject.SetActive(false);
        }

        private void FinalizeShard()
        {
            transform.parent = null;
            gameObject.SetActive(true);
        }

        private void FixMesh(IReadOnlyList<Vector3> verts, IReadOnlyList<int> tris)
        {
            try
            {
                var i1 = 0;
                var v3 = new Vector3[tris.Count];
                var v2 = new Vector2[tris.Count];
                var numArray = new int[tris.Count];

                for (var i = 0; i < tris.Count; i += 3)
                {
                    v3[i1] = verts[tris[i]];
                    numArray[i1] = i1;

                    v2[i1] = new Vector2(1f, 0.0f);
                    var i2 = i1 + 1;

                    v3[i2] = verts[tris[i + 1]];
                    numArray[i2] = i2;

                    v2[i2] = new Vector2(0.0f, 0.0f);
                    var i3 = i2 + 1;

                    v3[i3] = verts[tris[i + 2]];
                    numArray[i3] = i3;

                    v2[i3] = new Vector2(1f, 1f);
                    i1 = i3 + 1;
                }

                Mesh.vertices = v3;
                Mesh.triangles = numArray;
                Mesh.uv = v2;

                Mesh.RecalculateNormals();
                _meshCollider.sharedMesh = Mesh;
                FinalizeShard();
            }
            catch
            {
                // ignore
            }
        }

        internal static Shard CreateShard(Renderer renderer, Vector3[] newVertices, int[] newTriangles)
        {
            var nextShard = ShardPool.NextShard;
            if (nextShard == null)
            {
                Debug.LogError("Not enough shards left in shard pool.");
                return null;
            }

            nextShard.name = "Split shard";
            nextShard.tag = "Bullet";
            nextShard.gameObject.layer = 7;
            nextShard.FixMesh(newVertices, newTriangles);
            nextShard._renderer.material = renderer.material;
            return nextShard;
        }
    }
}