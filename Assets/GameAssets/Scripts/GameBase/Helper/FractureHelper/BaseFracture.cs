﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Interface;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    public abstract class BaseFracture : MonoBehaviour, IDestruction
    {
        [Header("Uses voronoi partitioning to fracture a box into smaller, convex shards.")]
        [Header("Amount of shards")]
        [SerializeField]
        protected RangedInt shards = new RangedInt(2, 2);

        [SerializeField] private bool immediate = true;

        [ShowIf(nameof(immediate), false)] [SerializeField]
        protected int shardsPerFrame = 2;

        [ReadOnly] public List<Shard> finishedShards = new List<Shard>();
        
        protected readonly List<Vector4> planes = new List<Vector4>(100);
        protected static readonly Vector4[] InitPlanes =
        {
            Vector3.right,
            Vector3.up,
            Vector3.forward,
            Vector3.left,
            Vector3.down,
            Vector3.back
        };

        protected List<Vector3> vertices = new List<Vector3>(50);
        protected List<int> planeIndices = new List<int>(100);

        private Rigidbody _rb;
        private Collider _collider;
        private MeshRenderer _renderer;

        protected Vector3 maxBounds;
        protected Vector3 minBounds;
        protected Vector3[] newVertices;

        protected int[] newTriangles;

        private bool _complete;

        protected abstract void SpreadFracture(Vector3[] points);

        protected abstract void ImmediateFracture(Vector3[] points);

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
            _renderer = GetComponentInChildren<MeshRenderer>();
        }

        private void BeginFracture(Vector3 point, float size = 0.5f)
        {
            if (_complete)
            {
                return;
            }

            _complete = true;

            var points = new Vector3[shards.Random()];
            for (var index = 0; index < points.Length; ++index)
            {
                var vector3 = Random.insideUnitSphere * size;
                points[index] = transform.InverseTransformPoint(point) + vector3;
            }

            if (immediate)
            {
                ImmediateFracture(points);
            }
            else
            {
                SpreadFracture(points);
            }
        }

        public void Destroy(Vector3 point, float force) => BeginFracture(point, force);

        protected void InitializeDestruction()
        {
            maxBounds = transform.lossyScale / 2f;
            minBounds = -maxBounds;
            _collider.enabled = false;

            if (!(bool) (Object) _rb)
            {
                return;
            }

            _rb.isKinematic = true;
        }

        protected void FinalizeDestruction()
        {
            foreach (var x in finishedShards)
            {
                x.GetComponent<Rigidbody>().isKinematic = false;
            }

            Destroy(gameObject);
        }

        protected void GetVerticesInPlane()
        {
            vertices.Clear();
            planeIndices.Clear();
            for (var i = 0; i < planes.Count; ++i)
            {
                for (var j = i + 1; j < planes.Count; ++j)
                {
                    var v1 = Vector3.Cross(planes[i], planes[j]);
                    if (!(v1.sqrMagnitude > 1.0 / 1000.0))
                    {
                        continue;
                    }

                    for (var k = j + 1; k < planes.Count; ++k)
                    {
                        var cross = Vector3.Cross(planes[j], planes[k]);
                        var v2 = Vector3.Cross(planes[k],
                            planes[i]);
                        if (!(cross.sqrMagnitude > 1.0 / 1000.0) || !(v2.sqrMagnitude > 1.0 / 1000.0))
                        {
                            continue;
                        }

                        var f = Vector3.Dot(planes[i], cross);

                        if (!(Mathf.Abs(f) > 1.0 / 1000.0))
                        {
                            continue;
                        }

                        var cross2 = (cross * planes[i].w + v2 * planes[j].w + v1 * planes[k].w) * (-1f / f);
                        int l;

                        for (l = 0; l < planes.Count; ++l)
                        {
                            var plane = planes[l];
                            if (Vector3.Dot(plane, cross2) + (double) plane.w > 1.0 / 1000.0)
                            {
                                break;
                            }
                        }

                        if (!l.Equals(planes.Count))
                        {
                            continue;
                        }

                        vertices.Add(cross2);

                        planeIndices.Add(i);
                        planeIndices.Add(j);
                        planeIndices.Add(k);
                    }
                }
            }

            vertices = vertices.DedupeCollectionWithRandom();
            planeIndices = planeIndices.DedupeCollection();
            planeIndices.Sort();
        }

        protected void CreateShardMesh(Vector3 point)
        {
            var shard = Shard.CreateShard(_renderer, newVertices, newTriangles);
            if (shard == null)
            {
                return;
            }

            var t = transform;
            shard.transform.SetPositionAndRotation(t.position + t.TransformDirection(point), t.rotation);

            var rb = shard.GetComponent<Rigidbody>();
            rb.mass = _rb.mass / shards.Random();
            rb.isKinematic = true;
            finishedShards.Add(shard);
        }
    }
}