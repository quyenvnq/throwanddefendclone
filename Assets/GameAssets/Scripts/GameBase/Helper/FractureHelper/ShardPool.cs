﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    public class ShardPool : BaseSingleton<ShardPool>
    {
        [SerializeField] private Shard[] shards;
        [SerializeField] private int maxPoolSize = 800;

        public static Shard NextShard => CanGetNextShard ? Instance.shards[Instance._nextFreeShard++] : null;

        private static bool CanGetNextShard => Instance._nextFreeShard + 1 < Instance.shards.Length;

        private int _nextFreeShard;

        public bool IsPopulated => shards != null;

        protected override void Awake()
        {
            base.Awake();
            PopulatePool();
        }

        public void PopulatePool()
        {
            if (IsPopulated && shards.Length.Equals(maxPoolSize))
            {
                return;
            }

            if (shards != null)
            {
                foreach (var x in shards)
                {
                    DestroyImmediate(x.gameObject);
                }
            }

            shards = new Shard[maxPoolSize];
            for (var index = 0; index < maxPoolSize; ++index)
            {
                shards[index] = new GameObject("Shard").AddComponent<Shard>();
                shards[index].transform.SetParent(transform);
            }
        }
    }
}