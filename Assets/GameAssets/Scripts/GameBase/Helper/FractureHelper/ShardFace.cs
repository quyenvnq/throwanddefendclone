﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper.FractureHelper
{
    public class ShardFace
    {
        public static Vector3[] Points { get; set; }

        public int I0 { get; private set; }
        public int I1 { get; private set; }
        public readonly int i2;

        private float _a;
        private float _b;
        private float _c;
        private float _d;

        public Vector3 Centroid
        {
            get
            {
                var point1 = Points[I0];
                var point2 = Points[I1];
                var point3 = Points[i2];
                return new Vector3(point1.x + point2.x + point3.x, point1.y + point2.y + point3.y,
                    point1.z + point2.z + point3.z) / 3f;
            }
        }

        public ShardFace(int i0, int i1, int i2)
        {
            I0 = i0;
            I1 = i1;
            this.i2 = i2;
            ComputePlane();
        }

        private void ComputePlane()
        {
            var point1 = Points[I0];
            var point2 = Points[I1];
            var point3 = Points[i2];

            _a = (float) (point1.y * (point2.z - (double) point3.z) + point2.y * (point3.z - (double) point1.z) +
                          point3.y * (point1.z - (double) point2.z));
            _b = (float) (point1.z * (point2.x - (double) point3.x) + point2.z * (point3.x - (double) point1.x) +
                          point3.z * (point1.x - (double) point2.x));
            _c = (float) (point1.x * (point2.y - (double) point3.y) + point2.x * (point3.y - (double) point1.y) +
                          point3.x * (point1.y - (double) point2.y));
            _d = (float) -(point1.x * (point2.y * (double) point3.z - point3.y * (double) point2.z) +
                           point2.x * (point3.y * (double) point1.z - point1.y * (double) point3.z) +
                           point3.x * (point1.y * (double) point2.z - point2.y * (double) point1.z));
        }

        public bool IsVisible(Vector3 p) => _a * (double) p.x + _b * (double) p.y + _c * (double) p.z + _d >= 0.0;

        public void Flip()
        {
            var i = I0;
            I0 = I1;
            I1 = i;
            ComputePlane();
        }
    }
}