﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public class SpawnerHelper : MonoBehaviour
    {
        #region Handle Pooling

        private static readonly Dictionary<string, Queue<GameObject>> Pooling =
            new Dictionary<string, Queue<GameObject>>();

        private static GameObject GetObject(GameObject go, Transform parent, bool isAlwaysCreate = false)
        {
            if (!Pooling.TryGetValue(go.name, out var queue))
            {
                return CreateNewObject(go, parent);
            }

            if (queue.Count == 0)
            {
                return CreateNewObject(go, parent);
            }

            var clone = queue.Dequeue();

            if (clone == null || isAlwaysCreate)
            {
                clone = CreateNewObject(go, parent);
            }

            clone.SetActive(true);
            return clone;
        }

        private static GameObject CreateNewObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, parent);
            clone.name = go.name;
            return clone;
        }

        private static async void DestroyObject(GameObject go, float timeDestroy)
        {
            await Task.Delay(TimeSpan.FromSeconds(timeDestroy));
            
            if (go == null)
            {
                return;
            }

            if (Pooling.TryGetValue(go.name, out var queue))
            {
                queue.Enqueue(go);
            }
            else
            {
                var newQueue = new Queue<GameObject>();
                newQueue.Enqueue(go);
                Pooling.Add(go.name, newQueue);
            }

            go.SetActive(false);
        }

        #endregion

        public static GameObject CreateSpawner(Vector3 position, Transform parent, GameObject newPrefab = null,
            bool isSetLocal = false, bool isAlwaysCreate = false)
        {
            var go = GetObject(newPrefab, parent, isAlwaysCreate);

            if (isSetLocal)
            {
                go.transform.localPosition = position;
            }
            else
            {
                go.transform.position = position;
            }

            return go;
        }

        public static void DestroySpawner(GameObject go, float timeDestroy = 0f)
        {
            DestroyObject(go, timeDestroy);
        }

        public static GameObject CreateAndDestroy(Vector3 position, Transform parent,
            GameObject newPrefab = null, float timeDestroy = 0f, bool isSetLocal = false)
        {
            var clone = CreateSpawner(position, parent, newPrefab, isSetLocal);
            DestroySpawner(clone, timeDestroy);
            return clone;
        }
    }
}