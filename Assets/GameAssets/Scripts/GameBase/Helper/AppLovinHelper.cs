﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Manager;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Helper
{
    [System.Serializable]
    public class GameEventStart
    {
        public int level_number;
        public string level_name;
        public int level_count;
        public string level_diff;
        public int level_loop;
        public bool level_random;
        public string level_type;
        public string game_mode;

        public GameEventStart()
        {
        }

        public GameEventStart(int number, string name, int count, string diff, int loop, bool random, string type,
            string mode)
        {
            this.level_number = number;
            this.level_name = name;
            this.level_count = count;
            this.level_diff = diff;
            this.level_loop = loop;
            this.level_random = random;
            this.level_type = type;
            this.game_mode = mode;
        }
    }

    [System.Serializable]
    public class GameEventFinish
    {
        public int level_number;
        public string level_name;
        public int level_count;
        public string level_diff;
        public int level_loop;
        public bool level_random;
        public string level_type;
        public string game_mode;
        public string result;
        public int time;
        public int progress;
        public int continuee;

        public GameEventFinish()
        {
        }

        public GameEventFinish(int number, string name, int count, string diff, int loop, bool random, string type,
            string mode, string mResult, int mTime, int mProgress, int mContinuee)
        {
            this.level_number = number;
            this.level_name = name;
            this.level_count = count;
            this.level_diff = diff;
            this.level_loop = loop;
            this.level_random = random;
            this.level_type = type;
            this.game_mode = mode;
            this.result = mResult;
            this.time = mTime;
            this.progress = mProgress;
            this.continuee = mContinuee;
        }
    }

    [System.Serializable]
    public class AdsEvent
    {
        public string ad_type;
        public string placement;
        public string result;
        public bool connection;

        public AdsEvent()
        {
        }

        public AdsEvent(string type, string placement, string result)
        {
            this.ad_type = type;
            this.placement = placement;
            this.result = result;
            this.connection = (Application.internetReachability != NetworkReachability.NotReachable);
        }
    }

    [System.Serializable]
    public class PaymentEvent
    {
        public string inapp_id;
        public string currency;
        public float price;
        public string inapp_type;

        public PaymentEvent()
        {
        }

        public PaymentEvent(string id, string currency, float price, string type)
        {
            this.inapp_id = id;
            this.currency = currency;
            this.price = price;
            this.inapp_type = type;
        }
    }

    [System.Serializable]
    public class RateEvent
    {
        public string show_reason;
        public int rate_result;

        public RateEvent(string reason, int rate)
        {
            this.show_reason = reason;
            this.rate_result = rate;
        }

        public RateEvent()
        {
        }
    }

    [System.Serializable]
    public class SkinEvent
    {
        public string skin_type;
        public string skin_name;
        public string skin_rarity;
        public string unlock_type;

        public SkinEvent(string type, string skinName, string rarity, string unlockType)
        {
            this.skin_type = type;
            this.skin_name = skinName;
            this.skin_rarity = rarity;
            this.unlock_type = unlockType;
        }

        public SkinEvent()
        {
        }
    }

    public static class AdsConfig
    {
        public static string MaxSdkKey =
            "6AQkyPv9b4u7yTtMH9PT40gXg00uJOTsmBOf7hDxa_-FnNZvt_qTLnJAiKeb5-2_T8GsI_dGQKKKrtwZTlCzAR";

        public static string InterstitialAdUnitId = "289d1eebbf85c6cc";
        public static string RewardedAdUnitId = "150086f696ee891e";
        public static string RewardedInterstitialAdUnitId = "ENTER_REWARD_INTER_AD_UNIT_ID_HERE";
        public static string BannerAdUnitId = "1bc90dd5f2bc40b7";
        public static string MRecAdUnitId = "ENTER_MREC_AD_UNIT_ID_HERE";
    }

    public static class AdsStatus
    {
        //public static bool RewardReady()
        //{
        //    return MaxSdk.IsRewardedAdReady(AdsConfig.RewardedAdUnitId);
        //}

        //public static bool InterstitialReady()
        //{
        //    return MaxSdk.IsInterstitialReady(AdsConfig.InterstitialAdUnitId);
        //}
    }

    public static class AppMetricaEvent
    {
        public static void SendDataEvent<T>(string eventName, T obj)
        {
            AppMetrica.Instance.ReportEvent(eventName, JsonUtility.ToJson(obj));
            Debug.Log("App Metrica Event ==> " + eventName + " : " + JsonUtility.ToJson(obj));
            AppMetrica.Instance.SendEventsBuffer();
        }

        public static void SendDataEvent(string eventName, string json)
        {
            AppMetrica.Instance.ReportEvent(eventName, json);
            Debug.Log("App Metrica Event ==> " + eventName + " : " + json);
            AppMetrica.Instance.SendEventsBuffer();
        }

        public static void SendDataEvent(string eventName, Dictionary<string, object> data)
        {
            AppMetrica.Instance.ReportEvent(eventName, data);
            /*Debug.Log("App Metrica Event ==> " + eventName + " : " + json);*/
            AppMetrica.Instance.SendEventsBuffer();
        }

        public static string DictionaryToJson(Dictionary<string, string> dict)
        {
            var entries = dict.Select(d =>
                $"\"{d.Key}\": \"{string.Join(",", d.Value)}\"");
            return "{" + string.Join(",", entries) + "}";
        }

        public static void SendEventFinish(string result)
        {
            var gef = new GameEventFinish
            {
                level_number = UserDataManager.Instance.userDataSave.level,
                level_name = $"Level {UserDataManager.Instance.userDataSave.level}",
                level_count = 0,
                level_diff = "normal",
                level_loop = 0,
                level_random = false,
                level_type = "normal",
                game_mode = "classic",
                result = result,
                time = 1,
                progress = 100,
                continuee = 1
            };

            var eventEnd = new Dictionary<string, string>
            {
                {"level_number", $"{gef.level_number}"},
                {"level_name", gef.level_name},
                {"level_count", $"{gef.level_count}"},
                {"level_diff", gef.level_diff},
                {"level_loop", $"{gef.level_loop}"},
                {"level_random", $"{gef.level_random}"},
                {"level_type", gef.level_type},
                {"game_mode", gef.game_mode},
                {"result", gef.result},
                {"time", $"{gef.time}"},
                {"progress", $"{gef.progress}"},
                {"continue", $"{gef.continuee}"}
            };

            SendDataEvent("level_finish", DictionaryToJson(eventEnd));
        }

        public static void SendEventStart()
        {
            var ges = new GameEventStart
            {
                level_number = UserDataManager.Instance.userDataSave.level,
                level_name = $"Level {UserDataManager.Instance.userDataSave.level}",
                level_count = 0,
                level_diff = "normal",
                level_loop = 0,
                level_random = false,
                level_type = "normal",
                game_mode = "classic"
            };
            
            var eventStart = new Dictionary<string, string>
            {
                {"level_number", $"{ges.level_number}"},
                {"level_name", ges.level_name},
                {"level_count", $"{ges.level_count}"},
                {"level_diff", ges.level_diff},
                {"level_loop", $"{ges.level_loop}"},
                {"level_random", $"{ges.level_random}"},
                {"level_type", ges.level_type},
                {"game_mode", ges.game_mode}
            };

            SendDataEvent("level_start", DictionaryToJson(eventStart));
        }
    }

    public class AppLovinHelper : BaseSingleton<AppLovinHelper>
    {
        private bool isBannerShowing;
        private bool isMRecShowing;
        private int interstitialRetryAttempt;
        private int rewardedRetryAttempt;
        private int rewardedInterstitialRetryAttempt;

        private bool m_isCantShowAds = false;
        public System.Action m_rewardAction;
        public System.Action m_rewardFailedAction;

        [SerializeField] GameObject m_AdsPopsLoading;
        [SerializeField] GameObject m_AdsFailedPops;
        [SerializeField] Image m_loadingImg;
        [SerializeField] Button m_btnCloseAdsPops;

        [Header("")] [SerializeField] GameObject m_Pops;
        [SerializeField] Button m_btnAccept;
        [SerializeField] Button m_btnTerms;
        [SerializeField] Button m_btnPolicy;

        //MaxSdkBase.SdkConfiguration sdkConfiguration;
        private void Start()
        {
            /*m_Pops.SetActive(false);
            EnableAdsLoading(false);
            m_AdsFailedPops.SetActive(false);
            m_btnCloseAdsPops.onClick.AddListener(() =>
            {
                //EnableAdsLoading(false);
            });
            //Debug.Log(JsonUtility.ToJson(sdkConfiguration));
            m_btnAccept.onClick.AddListener(() =>
            {
                PlayerPrefs.SetInt("ConsentApplies", 1);
                m_Pops.SetActive(false);
                //MaxSdk.SetHasUserConsent(true);
                //FindObjectOfType<UIFade>().FadeOn();
                SceneManager.LoadScene("GamePlay");
            });
            m_btnPolicy.onClick.AddListener(() =>
            {
                Application.OpenURL("https://sifodyasgames.com/policy#h.hn0lb3lfd0ij");
            });
            m_btnTerms.onClick.AddListener(() =>
            {
                Application.OpenURL("https://sifodyasgames.com/policy#h.v7mztoso1wgw");
            });*/
            //showInterstitialButton.onClick.AddListener(ShowInterstitial);
            //showRewardedButton.onClick.AddListener(ShowRewardedAd);
            //showRewardedInterstitialButton.onClick.AddListener(ShowRewardedInterstitialAd);
            //showBannerButton.onClick.AddListener(ToggleBannerVisibility);
            //showMRecButton.onClick.AddListener(ToggleMRecVisibility);
            //mediationDebuggerButton.onClick.AddListener(MaxSdk.ShowMediationDebugger);
            OnConsentFlow();
        }

        bool loadingRot;
        private bool m_rewarded;

        [Button]
        public void EnableAdsLoading(bool isActive)
        {
            m_AdsFailedPops.SetActive(false);
            if (isActive)
            {
                loadingRot = true;
                m_AdsPopsLoading.SetActive(true);
            }
            else
            {
                loadingRot = false;
                m_loadingImg.transform.localEulerAngles = Vector3.zero;
                m_AdsPopsLoading.SetActive(false);
                Debug.Log("Disable Log");
            }
        }

        public void EnablePops()
        {
            if (m_AdsPopsLoading.activeInHierarchy)
            {
                m_AdsFailedPops.SetActive(true);
            }
        }

        public void OnConsentFlow()
        {
            //if (PlayerPrefs.GetInt("ConsentApplies") == 1)
            //{
            //    MaxSdk.SetHasUserConsent(true);
            //}
            //MaxSdkCallbacks.OnSdkInitializedEvent += ConfigCallback;
            //MaxSdk.SetSdkKey(AdsConfig.MaxSdkKey);
            //MaxSdk.InitializeSdk();
        }

        //private void ConfigCallback(MaxSdkBase.SdkConfiguration obj)
        //{
        //    if (obj.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies)
        //    {
        //        // Show user consent dialog
        //        if (MaxSdk.HasUserConsent())
        //        {
        //            m_Pops.SetActive(false);
        //            FindObjectOfType<UIFade>().FadeOff();
        //        }
        //        else
        //        {
        //            m_Pops.SetActive(true);
        //        }
        //    }
        //    else
        //    {
        //        m_Pops.SetActive(false);

        //        FindObjectOfType<UIFade>().FadeOff();
        //    }
        //    OnLoadAds();
        //}

        //void OnLoadAds()
        //{
        //    InitializeRewardedAds();
        //    if (!UserDataManager.I.m_userDataSave.m_noAds)
        //    {
        //        InitializeBannerAds();
        //        InitializeInterstitialAds();
        //    }
        //}
        //public bool CanShowAds()
        //{
        //    return !m_isCantShowAds;
        //}
        //public void OnCountTimeAds()
        //{
        //    m_isCantShowAds = true;
        //    float mtime = 0;
        //    DOVirtual.DelayedCall(45, () =>
        //    {
        //        m_isCantShowAds = false;
        //    });
        //}

        //#region Interstitial Ad Methods

        //private void InitializeInterstitialAds()
        //{
        //    if (UserDataManager.I.m_userDataSave.m_noAds)
        //        return;
        //    MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        //    MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        //    MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        //    MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;
        //    MaxSdkCallbacks.OnInterstitialDisplayedEvent += OnInterstitialDisplayedEvent;
        //    LoadInterstitial();
        //}


        //void LoadInterstitial()
        //{
        //    if (!UserDataManager.I.m_userDataSave.m_noAds)
        //        MaxSdk.LoadInterstitial(AdsConfig.InterstitialAdUnitId);
        //}

        //public void ShowInterstitial()
        //{
        //    if (UserDataManager.I.m_userDataSave.m_noAds) return;
        //    if (m_isCantShowAds) return;
        //    //EnableAdsLoading(true);
        //    //if (Application.internetReachability == NetworkReachability.NotReachable)
        //    //{
        //    //    EnablePops();
        //    //}
        //    //else
        //    //{
        //    if (MaxSdk.IsInterstitialReady(AdsConfig.InterstitialAdUnitId))
        //    {
        //        if (!UserDataManager.I.m_userDataSave.m_noAds)
        //        {
        //            AppmetricEvent.SendDataEvent("video_ads_available", new AdsEvent("interstitial", "ad_on_replay", AdsStatus.InterstitialReady() ? "success" : "not_avaiable"));
        //            MaxSdk.ShowInterstitial(AdsConfig.InterstitialAdUnitId);
        //            OnCountTimeAds();
        //        }

        //    }
        //    else
        //    {

        //    }
        //    //}
        //}
        //private void OnInterstitialDisplayedEvent(string obj)
        //{
        //    AppmetricEvent.SendDataEvent("video_ads_started", new AdsEvent("interstitial", "ad_on_relay", "start"));
        //}
        //private void OnInterstitialLoadedEvent(string adUnitId)
        //{
        //    // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
        //    Debug.Log("Interstitial loaded");
        //    // Reset retry attempt
        //    interstitialRetryAttempt = 0;
        //}

        //private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
        //{
        //    // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        //    interstitialRetryAttempt++;
        //    double retryDelay = Mathf.Pow(2, Mathf.Min(6, interstitialRetryAttempt));
        //    Debug.Log("Interstitial failed to load with error code: " + errorCode);
        //    Invoke("LoadInterstitial", (float)retryDelay);
        //}

        //private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
        //{
        //    // Interstitial ad failed to display. We recommend loading the next ad
        //    Debug.Log("Interstitial failed to display with error code: " + errorCode);
        //    EnablePops();
        //    LoadInterstitial();
        //}

        //private void OnInterstitialDismissedEvent(string adUnitId)
        //{
        //    AppmetricEvent.SendDataEvent("video_ads_watch", new AdsEvent("interstitial", "ad_on_relay", "watched"));
        //    // Interstitial ad is hidden. Pre-load the next ad
        //    Debug.Log("Interstitial dismissed");
        //    //EnableAdsLoading(false);
        //    LoadInterstitial();

        //}

        //#endregion

        //#region Rewarded Ad Methods

        //private void InitializeRewardedAds()
        //{
        //    // Attach callbacks
        //    MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        //    MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        //    MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        //    MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        //    MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
        //    MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        //    MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        //    // Load the first RewardedAd
        //    LoadRewardedAd();
        //}

        //private void LoadRewardedAd()
        //{
        //    MaxSdk.LoadRewardedAd(AdsConfig.RewardedAdUnitId);
        //}

        //public void ShowRewardedAd(System.Action actionRewarded, System.Action actionRewardFailed = null)
        //{
        //    m_rewarded = false;
        //    //EnableAdsLoading(true);
        //    //if(Application.internetReachability == NetworkReachability.NotReachable)
        //    //{
        //    //    EnablePops();
        //    //}
        //    //else
        //    //{
        //    if (MaxSdk.IsRewardedAdReady(AdsConfig.RewardedAdUnitId))
        //    {
        //        AppmetricEvent.SendDataEvent("video_ads_available", new AdsEvent("rewarded", "get_shop_entry", AdsStatus.RewardReady() ? "success" : "not_avaiable"));
        //        m_rewardAction = actionRewarded;
        //        m_rewardFailedAction = actionRewardFailed;
        //        MaxSdk.ShowRewardedAd(AdsConfig.RewardedAdUnitId);
        //        OnCountTimeAds();
        //    }
        //    else
        //    {

        //    }
        //    //}

        //}

        //private void OnRewardedAdLoadedEvent(string adUnitId)
        //{
        //    // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
        //    Debug.Log("Rewarded ad loaded");
        //    EnablePops();
        //    // Reset retry attempt
        //    rewardedRetryAttempt = 0;
        //}

        //private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
        //{
        //    // Rewarded ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        //    rewardedRetryAttempt++;
        //    double retryDelay = Mathf.Pow(2, Mathf.Min(6, rewardedRetryAttempt));
        //    Debug.Log("Rewarded ad failed to load with error code: " + errorCode);
        //    Invoke("LoadRewardedAd", (float)retryDelay);
        //    EnablePops();
        //}

        //private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
        //{
        //    // Rewarded ad failed to display. We recommend loading the next ad
        //    Debug.Log("Rewarded ad failed to display with error code: " + errorCode);
        //    //EnableAdsLoading(false);
        //    LoadRewardedAd();
        //}

        //private void OnRewardedAdDisplayedEvent(string adUnitId)
        //{
        //    AppmetricEvent.SendDataEvent("video_ads_started", new AdsEvent("rewarded", "get_shop_entry", "start"));
        //    Debug.Log("Rewarded ad displayed");
        //    //EnableAdsLoading(false);
        //}

        //private void OnRewardedAdClickedEvent(string adUnitId)
        //{
        //    Debug.Log("Rewarded ad clicked");
        //}

        //private void OnRewardedAdDismissedEvent(string adUnitId)
        //{
        //    AppmetricEvent.SendDataEvent("video_ads_watch", new AdsEvent("rewarded", "get_shop_entry", "watched"));
        //    // Rewarded ad is hidden. Pre-load the next ad
        //    Debug.Log("Rewarded ad dismissed");
        //    EnablePops();

        //    LoadRewardedAd();
        //    if (m_rewarded)
        //    {
        //        m_rewardAction();
        //        m_rewarded = false;
        //    }
        //    else{
        //        if (m_rewardFailedAction != null)
        //            m_rewardFailedAction();
        //    }
        //}

        //private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        //{
        //    //EnableAdsLoading(false);
        //    m_rewarded = true;
        //    Debug.Log("Rewarded ad received reward");
        //}

        //#endregion

        #region Rewarded Interstitial Ad Methods

        //private void InitializeRewardedInterstitialAds()
        //{
        //    // Attach callbacks
        //    MaxSdkCallbacks.OnRewardedInterstitialAdLoadedEvent += OnRewardedInterstitialAdLoadedEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdLoadFailedEvent += OnRewardedInterstitialAdFailedEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdFailedToDisplayEvent += OnRewardedInterstitialAdFailedToDisplayEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdDisplayedEvent += OnRewardedInterstitialAdDisplayedEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdClickedEvent += OnRewardedInterstitialAdClickedEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdHiddenEvent += OnRewardedInterstitialAdDismissedEvent;
        //    MaxSdkCallbacks.OnRewardedInterstitialAdReceivedRewardEvent += OnRewardedInterstitialAdReceivedRewardEvent;

        //    // Load the first RewardedInterstitialAd
        //    LoadRewardedInterstitialAd();
        //}

        //private void LoadRewardedInterstitialAd()
        //{
        //    AppMetrica.Instance.ReportEvent("LoadRewardedInterstitialAd");
        //    MaxSdk.LoadRewardedInterstitialAd(AdsConfig.RewardedInterstitialAdUnitId);
        //}

        //private void ShowRewardedInterstitialAd()
        //{
        //    if (MaxSdk.IsRewardedInterstitialAdReady(AdsConfig.RewardedInterstitialAdUnitId))
        //    {
        //        MaxSdk.ShowRewardedInterstitialAd(AdsConfig.RewardedInterstitialAdUnitId);
        //    }
        //    else
        //    {

        //    }
        //}

        private void OnRewardedInterstitialAdLoadedEvent(string adUnitId)
        {
            // Rewarded interstitial ad is ready to be shown. MaxSdk.IsRewardedInterstitialAdReady(rewardedInterstitialAdUnitId) will now return 'true'
            Debug.Log("Rewarded interstitial ad loaded");

            // Reset retry attempt
            rewardedInterstitialRetryAttempt = 0;
        }

        //private void OnRewardedInterstitialAdFailedEvent(string adUnitId, int errorCode)
        //{
        //    // Rewarded interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        //    rewardedInterstitialRetryAttempt++;
        //    double retryDelay = Mathf.Pow(2, Mathf.Min(6, rewardedInterstitialRetryAttempt));

        //    Debug.Log("Rewarded interstitial ad failed to load with error code: " + errorCode);

        //    Invoke("LoadRewardedInterstitialAd", (float)retryDelay);
        //}

        //private void OnRewardedInterstitialAdFailedToDisplayEvent(string adUnitId, int errorCode)
        //{
        //    // Rewarded interstitial ad failed to display. We recommend loading the next ad
        //    Debug.Log("Rewarded interstitial ad failed to display with error code: " + errorCode);
        //    LoadRewardedInterstitialAd();
        //}

        private void OnRewardedInterstitialAdDisplayedEvent(string adUnitId)
        {
            Debug.Log("Rewarded interstitial ad displayed");
        }

        private void OnRewardedInterstitialAdClickedEvent(string adUnitId)
        {
            Debug.Log("Rewarded interstitial ad clicked");
        }

        //private void OnRewardedInterstitialAdDismissedEvent(string adUnitId)
        //{
        //    // Rewarded interstitial ad is hidden. Pre-load the next ad
        //    Debug.Log("Rewarded interstitial ad dismissed");
        //    LoadRewardedInterstitialAd();
        //}

        //private void OnRewardedInterstitialAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        //{
        //    // Rewarded interstitial ad was displayed and user should receive the reward
        //    Debug.Log("Rewarded interstitial ad received reward");
        //}

        #endregion

        #region Banner Ad Methods

        //private void InitializeBannerAds()
        //{
        //    if (UserDataManager.I.m_userDataSave.m_noAds)
        //        return;
        //    // Banners are automatically sized to 320x50 on phones and 728x90 on tablets.
        //    // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments.
        //    MaxSdk.CreateBanner(AdsConfig.BannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);
        //    // Set background or background color for banners to be fully functional.
        //    MaxSdk.SetBannerBackgroundColor(AdsConfig.BannerAdUnitId, Color.black);

        //    MaxSdkCallbacks.OnBannerAdLoadedEvent += OnBannerAdLoadedEvent;
        //}

        //private void OnBannerAdLoadedEvent(string obj)
        //{
        //    if(!UserDataManager.I.m_userDataSave.m_noAds)
        //    {
        //        AppmetricEvent.SendDataEvent("video_ads_watch", new AdsEvent("banner", "ad_on_replay", "watched"));
        //        MaxSdk.ShowBanner(AdsConfig.BannerAdUnitId);
        //    }
        //    else
        //    {
        //        MaxSdk.HideBanner(AdsConfig.BannerAdUnitId);
        //    }
        //}

        //private void ToggleBannerVisibility()
        //{
        //    if (!isBannerShowing)
        //    {
        //        MaxSdk.ShowBanner(AdsConfig.BannerAdUnitId);
        //    }
        //    else
        //    {
        //        MaxSdk.HideBanner(AdsConfig.BannerAdUnitId);
        //    }

        //    isBannerShowing = !isBannerShowing;
        //}
        //public void OnHideBanner()
        //{
        //    MaxSdk.HideBanner(AdsConfig.BannerAdUnitId);
        //}

        #endregion

        #region MREC Ad Methods

        //private void InitializeMRecAds()
        //{
        //    // MRECs are automatically sized to 300x250.
        //    MaxSdk.CreateMRec(AdsConfig.MRecAdUnitId, MaxSdkBase.AdViewPosition.BottomCenter);
        //}

        //private void ToggleMRecVisibility()
        //{
        //    if (!isMRecShowing)
        //    {
        //        MaxSdk.ShowMRec(AdsConfig.MRecAdUnitId);
        //    }
        //    else
        //    {
        //        MaxSdk.HideMRec(AdsConfig.MRecAdUnitId);
        //    }

        //    isMRecShowing = !isMRecShowing;
        //}

        #endregion
    }
}