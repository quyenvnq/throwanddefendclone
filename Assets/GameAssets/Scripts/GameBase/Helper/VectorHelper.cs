﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class VectorHelper
    {
        public static bool Distance(this Component a, Component b, float minDistance)
        {
            return (a.transform.position - b.transform.position).sqrMagnitude <= minDistance * minDistance;
        }

        public static Vector3 GetPlatformPosition
        {
            get
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.WindowsPlayer:
                        return Input.mousePosition;
                    
                    case RuntimePlatform.Android:
                    case RuntimePlatform.IPhonePlayer:
                        return Input.touchCount > 0 ? (Vector3) Input.GetTouch(0).position : Vector3.zero;
                    
                    default:
                        return Vector3.zero;
                }
            }
        }

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Quaternion ToQuaternion(this Vector3 v)
        {
            var angle = Mathf.Atan2(v.x, v.z);
            return new Quaternion(v.x * Mathf.Sin(angle / 2), v.y * Mathf.Sin(angle / 2), v.z * Mathf.Sin(angle / 2),
                Mathf.Cos(angle / 2));
        }

        public static Vector3 GetDirection(this Vector3 v)
        {
            var d = XYZDirection(v);
            
            switch (d)
            {
                case 0:
                    return Vector3.right;
                
                case 1:
                    return Vector3.up;
                
                case 2:
                    return Vector3.forward;
                
                default:
                    throw new InvalidOperationException();
            }
        }

        public static Vector3 GetDirectionFromInt(this int i)
        {
            switch (i)
            {
                case 0:
                    return Vector3.right;
                
                case 1:
                    return Vector3.up;
                
                case 2:
                    return Vector3.forward;
                
                default:
                    throw new InvalidOperationException();
            }
        }

        public static int XYZDirection(this Vector3 v)
        {
            var x = Mathf.Abs(v.x);
            var y = Mathf.Abs(v.y);
            var z = Mathf.Abs(v.z);
            return x > y & x > z ? 0 : y > x & y > z ? 1 : 2;
        }

        public static Vector3 RandomPos(this Bounds bounds, float scale = 1f)
        {
            var min = bounds.min;
            var max = bounds.max;
            return new Vector3(Random.Range(min.x * scale, max.x * scale),
                Random.Range(min.y * scale, max.y * scale),
                Random.Range(min.z * scale, max.z * scale));
        }
    }
}