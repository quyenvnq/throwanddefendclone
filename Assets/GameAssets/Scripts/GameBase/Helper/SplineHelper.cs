﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.GamePlay;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public class SplineHelper : BaseSingleton<SplineHelper>
    {
        [SerializeField] private Wall wall;

        private readonly List<Transform> _splines = new List<Transform>();

        private int _splineCount;

        private void Start()
        {
            _splineCount = transform.childCount;
            
            for (var i = 0; i < _splineCount; i++)
            {
                _splines.Add(transform.GetChild(i));
            }
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            if (_splineCount <= 1)
            {
                return;
            }

            for (var i = 1; i < _splineCount; i++)
            {
                Debug.DrawLine(_splines[i - 1].position, _splines[i].position, Color.green, Mathf.Infinity);
            }
        }

        public void MoveToSplinePoint(Rigidbody rb, ref int index, float moveSpeed, float minDistance)
        {
            var origin = rb.transform;
            var character = rb.GetComponent<BaseCharacter>();

            if (index > _splineCount - 1)
            {
                origin.LookAt(wall.transform);
                rb.MoveWithVelocity(wall.transform.position - origin.position, moveSpeed);
                return;
            }

            origin.LookAt(_splines[index].TransformDirection(Vector3.up * 5f));
            rb.MoveWithVelocity(_splines[index].position - origin.position, moveSpeed);

            if (!origin.Distance(_splines[index], minDistance))
            {
                return;
            }

            character.ResetVelocity();
            index++;
        }
    }
}