﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class ParticleSystemHelper
    {
        public static void SetActive(this ParticleSystem ps, bool active = true)
        {
            if (active)
            {
                ps.gameObject.SetActive(true);
                ps.Play();
            }
            else
            {
                var main = ps.main;
                main.stopAction = ParticleSystemStopAction.Disable;

                ps.gameObject.IgnoreRaycast();
                ps.Stop();
            }
        }
    }
}
