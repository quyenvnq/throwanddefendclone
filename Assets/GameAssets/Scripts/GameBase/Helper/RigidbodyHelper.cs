﻿using System;
using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class RigidbodyHelper
    {
        public static void SetAffectGravity(this Rigidbody rb, Collider c, bool active = true,
            bool isOnlyCollider = false)
        {
            if (!isOnlyCollider)
            {
                if (active)
                {
                    rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                }

                rb.isKinematic = active;
            }

            c.isTrigger = active;
        }

        public static IEnumerable<GameObject> SpawnerInsideCollider(this Collider c, List<GameObject> spawners,
            int amount)
        {
            var clones = new List<GameObject>();
            var index = 0;

            while (index < amount)
            {
                var rdBound = c.bounds.RandomPos();
                var rdPos3D = new Vector3(rdBound.x, rdBound.y, rdBound.z);

                var rdPos = c.ClosestPoint(rdPos3D);
                if (!rdPos.x.NearlyEqual(rdPos3D.x) || !rdPos.y.NearlyEqual(rdPos3D.y))
                {
                    continue;
                }

                var rd = Random.Range(0, spawners.Count - 1);
                var clone = SpawnerHelper.CreateSpawner(Vector3.zero, c.transform, spawners[rd], isAlwaysCreate: true);

                clone.transform.position = rdPos3D;
                clones.Add(clone);
                index++;
            }

            return clones;
        }

        public static void StopAgent(this NavMeshAgent nma)
        {
            if (nma == null || !nma.hasPath)
            {
                return;
            }

            nma.isStopped = true;
            nma.velocity = Vector3.zero;
            nma.ResetPath();
        }

        public static void ResetInertia(this Rigidbody rb)
        {
            rb.ResetInertiaTensor();
            rb.ResetCenterOfMass();

            rb.angularVelocity = Vector3.zero;
            rb.velocity = Vector3.zero;
        }

        public static void MoveWithVelocity(this Rigidbody rb, Vector3 direction, float moveSpeed)
        {
            rb.velocity = direction * (Time.fixedDeltaTime * moveSpeed);
        }

        public static void MoveToTarget(this Rigidbody rb, Transform target, float moveSpeed)
        {
            var originPos = rb.transform.position;
            var targetPos = target.position;
            rb.MovePosition(originPos + (targetPos - originPos) * (moveSpeed * Time.fixedDeltaTime));
        }

        public static Vector3 MoveDirection(this Rigidbody rb, Action onBegin = null,
            Action onMoving = null, Action onEnded = null, bool isOriginMoving = true)
        {
            var direction = Vector3.zero;
            var originPos = rb.transform.position;

            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
                case RuntimePlatform.Android:
                    if (Input.touchCount > 0)
                    {
                        var touch = Input.GetTouch(0);
                        switch (touch.phase)
                        {
                            case TouchPhase.Began:
                                if (isOriginMoving)
                                {
                                    direction = ScreenPositionInClick(touch.position);
                                }

                                if (IsPointerOverUIObject())
                                {
                                    return Vector3.zero;
                                }

                                onBegin?.Invoke();
                                break;

                            case TouchPhase.Moved:
                            case TouchPhase.Stationary:
                                if (IsPointerOverUIObject())
                                {
                                    return Vector3.zero;
                                }

                                if (isOriginMoving)
                                {
                                    direction = ScreenPositionInClick(touch.position);
                                }

                                onMoving?.Invoke();
                                break;

                            case TouchPhase.Ended:
                                if (IsPointerOverUIObject())
                                {
                                    return Vector3.zero;
                                }

                                rb.ResetInertia();
                                onEnded?.Invoke();
                                break;

                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    break;

                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (IsPointerOverUIObject())
                        {
                            return Vector3.zero;
                        }

                        if (isOriginMoving)
                        {
                            direction = ScreenPositionInClick(Input.mousePosition);
                        }

                        onBegin?.Invoke();
                    }
                    else if (Input.GetMouseButton(0))
                    {
                        if (IsPointerOverUIObject())
                        {
                            return Vector3.zero;
                        }

                        if (isOriginMoving)
                        {
                            direction = ScreenPositionInClick(Input.mousePosition);
                        }

                        onMoving?.Invoke();
                    }
                    else if (Input.GetMouseButtonUp(0))
                    {
                        if (IsPointerOverUIObject())
                        {
                            return Vector3.zero;
                        }

                        rb.ResetInertia();
                        onEnded?.Invoke();
                    }

                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            return direction - originPos;
        }

        private static Vector3 ScreenPositionInClick(this Vector3 position)
        {
            return !Physics.Raycast(GameManager.Instance.Camera.ScreenPointToRay(position),
                out var hit)
                ? Vector3.zero
                : new Vector3(hit.point.x, 0f, hit.point.z);
        }

        private static bool IsPointerOverUIObject()
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
            };

            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}