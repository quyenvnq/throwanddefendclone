﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class GameObjectHelper
    {
        public static void IgnoreRaycast(this GameObject go)
        {
            go.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
    }
}
