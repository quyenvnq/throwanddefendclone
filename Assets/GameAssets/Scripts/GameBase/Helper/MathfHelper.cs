﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.General;
using UnityEngine;
using SRandom = System.Random;
using URandom = UnityEngine.Random;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class MathfHelper
    {
        public static bool NearlyEqual(this float a, float b, float epsilon = 1f)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (a.Equals(b))
            {
                return true;
            }

            if (a.Equals(0) || b.Equals(0) || absA + absB < Const.MIN_NORMAL)
            {
                return diff < epsilon * Const.MIN_NORMAL;
            }

            return diff / (absA + absB) < epsilon;
        }

        public static float NextFloat(this SRandom rnd, float max, float min)
        {
            if (min > max)
            {
                return (float) rnd.NextDouble() * (min - max) + max;
            }

            return (float) rnd.NextDouble() * (max - min) + min;
        }

        public static double NextDouble(this SRandom rnd, double max, double min)
        {
            if (min > max)
            {
                return rnd.NextDouble() * (min - max) + max;
            }

            return rnd.NextDouble() * (max - min) + min;
        }

        public static bool NextBool => URandom.value > 0.5f;

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static int? GetIndexOfLowestLayer(this int layer)
        {
            for (var index = 0; index < sizeof(int) * 8; index++)
            {
                if ((layer & 1) == 1)
                {
                    return index;
                }

                layer >>= 1;
            }

            return null;
        }

        public static float MaxAbs(this float a, float b)
        {
            return Mathf.Abs(a) > Mathf.Abs(b) ? a : b;
        }

        public static float DropChance(this List<float> drops)
        {
            var range = drops.Sum();
            var rnd = URandom.Range(0, range);
            var top = 0f;

            foreach (var x in drops)
            {
                top += x;
                
                if (rnd < top)
                {
                    return x;
                }
            }

            return drops[0];
        }
    }
}