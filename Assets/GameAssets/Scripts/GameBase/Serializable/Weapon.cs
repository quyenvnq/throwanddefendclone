﻿using System;
using GameAssets.Scripts.GameBase.GamePlay;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Serializable
{
    [Serializable]
    public class Weapon
    {
        public Button btnWeapon;
        public Image imgProgressBar;
        public Sprite imgIcon;
        
        public GameObject prefab;
        public WeaponType weaponType;
        
        public float percentDrop;
        public int levelUnlock;
    }
}