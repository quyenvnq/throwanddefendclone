﻿using System;
using GameAssets.Scripts.GameBase.Manager;
using TMPro;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Serializable
{
    [Serializable]
    public class Upgrade
    {
        public Button btnUpgrade;
        public TextMeshProUGUI txtLevel;
        public TextMeshProUGUI txtPrice;

        public int baseLevel = 1;
        public int maxLevel;

        public int basePoint = 1;
        public int addPoint = 1;
        public float addPrice = 1;
        
        private float _basePrice = 1;

        public bool LevelUp()
        {
            if (baseLevel >= maxLevel || !UserDataManager.Instance.CheckMoney(_basePrice))
            {
                return false;
            }

            UserDataManager.Instance.AddMoney(-_basePrice, false);

            baseLevel++;
            _basePrice += addPrice;
            basePoint += addPoint;

            SetText();
            return true;
        }

        public void SetUpgrade(Upgrade u)
        {
            baseLevel = u.baseLevel;
            _basePrice = (u.baseLevel - 1) * u.addPrice + u.addPrice;
            basePoint = (u.baseLevel - 1) * u.addPoint + u.addPoint;
            SetText();
        }

        private void SetText()
        {
            var max = baseLevel >= maxLevel;
            if (max)
            {
                btnUpgrade.interactable = false;
            }

            txtPrice.text = !max ? $"{_basePrice}" : "MAX LEVEL";
            txtLevel.text = $"LVL{baseLevel}";
        }
    }
}