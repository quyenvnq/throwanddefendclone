﻿using System;
using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;

namespace GameAssets.Scripts.GameBase.Serializable
{
    [Serializable]
    public class UserDataSave
    {
        public int level;
        [ReadOnly] public int bestLevel;
        [ReadOnly] public int bestScore;
        [ReadOnly] public int bestScoreEndless;

        public float diamond;
        public float money;
        public float death;

        [ReadOnly] public float hours;
        [ReadOnly] public float minutes;
        [ReadOnly] public float seconds;

        public bool sound;
        public bool music;
        public bool vibrate;
        public bool removedAds;

        [ReadOnly] public string lastOpenDay;
        [ReadOnly] public string versionCode;

        public List<Highscore> highscores = new List<Highscore>();
        public List<Upgrade> upgrades = new List<Upgrade>();

        public string GetTimePlayed()
        {
            return $"{hours.Minimum10()}:{minutes.Minimum10()}:{seconds.Minimum10()}";
        }
    }
}