﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Serializable
{
    [Serializable]
    public class Level
    {
        public List<Weapon> unlockWeapons = new List<Weapon>();
        public GameObject levelPrefab;
        public int money;
    }
}