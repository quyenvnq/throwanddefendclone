﻿using System;
using GameAssets.Scripts.GameBase.GamePlay.Monster;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Serializable
{
    [Serializable]
    public class WaveMonster
    {
        public GameObject monsterPrefab;
        public int levelUnlock;
    }
}