﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public class Balance : MonoBehaviour
    {
        [SerializeField] private Transform body;

        private void Update()
        {
            transform.position = body.position;
        }
    }
}
