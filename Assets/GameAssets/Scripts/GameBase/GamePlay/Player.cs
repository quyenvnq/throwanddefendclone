﻿using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Manager.UI;
using GameAssets.Scripts.GameBase.Serializable;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public enum WeaponType
    {
        Arrow,
        Soldier,
        Stone
    }

    public class Player : BaseSingleton<Player>
    {
        [SerializeField] private LayerMask groundLayerMask;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject cursor;

        [SerializeField] private int lineSegmentCount = 20;
        [SerializeField] private float step = 2f;
        [SerializeField] private float thrustForce = 20f;
        [SerializeField] private float speedShoot = 49f;
        [SerializeField] private float radius = 15f;
        [SerializeField] private float timeRotationCamera = 0.0625f;

        [Header("Limit shoot")] [SerializeField]
        private float limitMinX = 200f;

        [SerializeField] private float limitMaxX = 2000f;
        [SerializeField] private float limitMinY = 200f;
        [SerializeField] private float limitMaxY = 2000f;

        private readonly List<Vector3> _linePoints = new List<Vector3>();
        private GameObject _bullet;
        private LineRenderer _line;
        private Rigidbody _rb;
        private WeaponType _bulletType;
        private UIGameStart _uiGameStart;

        private Vector3 _force;
        private Vector3 _origin;
        private Vector3 _destination;
        private Vector3 _direction;

        private int _amountBullet;

        private bool _isShooting;
        private bool _isCanShoot;

        protected override void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _line = GetComponentInChildren<LineRenderer>();
            _uiGameStart = Manager.UIManager.Instance.GetUI<UIGameStart>();
            Physics.gravity = new Vector3(0f, -speedShoot, 0f);
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            _rb.MoveDirection(
                () =>
                {
                    if (_isShooting)
                    {
                        return;
                    }

                    _isCanShoot = _uiGameStart.CanShootSoldier();
                    _origin = VectorHelper.GetPlatformPosition;
                },
                () =>
                {
                    if (_isShooting)
                    {
                        return;
                    }

                    if (_isCanShoot)
                    {
                        _destination = VectorHelper.GetPlatformPosition;
                        _force = _destination - _origin;

                        var t = transform.position;
                        var limitX = Mathf.Clamp(_force.x, t.x - limitMinX, t.x + limitMaxX);
                        var limitY = Mathf.Clamp(_force.y, t.y - limitMinY, t.y + limitMaxY);

                        _direction = new Vector3(limitX, limitY, limitY);
                        transform.DOLookAt(new Vector3(_direction.x, Mathf.Abs(t.y), _direction.z), timeRotationCamera);
                        DrawTrajectory(_direction * thrustForce, exitPoint);
                    }
                    else
                    {
                        DestroyTrajectory();
                    }
                },
                () =>
                {
                    if (_isCanShoot)
                    {
                        if (_isShooting || _bullet == null)
                        {
                            return;
                        }

                        _isShooting = true;
                        Invoke(nameof(DestroyTrajectory), 0.5f);
                        ShootBullet(_bulletType);
                    }
                    else
                    {
                        DestroyTrajectory();
                    }
                }, false);
        }

        private void ShootBullet(WeaponType weaponType)
        {
            var dir = new Vector3(_direction.x, Mathf.Abs(_direction.y), Mathf.Abs(_direction.z));
            _bullet.transform.SetParent(null);

            switch (weaponType)
            {
                case WeaponType.Arrow:
                    var arrow = _bullet.GetComponent<Arrow>();
                    arrow.MultiShoot(exitPoint.position, dir * thrustForce);
                    break;

                case WeaponType.Soldier:
                    ShootSoldier(_bullet.GetComponentInChildren<Soldier.Soldier>());
                    break;

                case WeaponType.Stone:
                    var stone = _bullet.GetComponent<Stone>();
                    stone.MultiShoot(exitPoint.position, dir * thrustForce);
                    break;
            }

            _uiGameStart.ChangeWeapon();
            _bullet = null;
        }

        private void ShootSoldier(Soldier.Soldier s)
        {
            var cursorPos = cursor.transform.position;
            s.SingleShoot(cursorPos, thrustForce, radius);

            for (var i = 0; i < _amountBullet - 1; i++)
            {
                var soldier = SpawnerHelper.CreateSpawner(exitPoint.position, null, s.transform.parent.gameObject)
                    .GetComponentInChildren<Soldier.Soldier>();
                soldier.SingleShoot(cursorPos, thrustForce, radius);
            }
        }

        public void CreateBullet(Weapon weapon)
        {
            if (_bullet != null)
            {
                SpawnerHelper.DestroySpawner(_bullet);
                _bullet = null;
            }

            switch (weapon.weaponType)
            {
                case WeaponType.Arrow:
                case WeaponType.Stone:
                    _bulletType = weapon.weaponType;
                    _amountBullet = 1;
                    break;

                default:
                    _bulletType = WeaponType.Soldier;
                    _amountBullet = UserDataManager.Instance.userDataSave.upgrades[0].basePoint;
                    break;
            }

            _bullet = SpawnerHelper.CreateSpawner(
                exitPoint.position + new Vector3(0, _bulletType.Equals(WeaponType.Arrow) ? 0.25f : -3f, 0),
                exitPoint, weapon.prefab, isAlwaysCreate: _bulletType.Equals(WeaponType.Stone));

            if (_bullet.transform.parent == null)
            {
                _bullet.transform.SetParent(exitPoint);
            }
        }

        private void DrawTrajectory(Vector3 force, Transform origin)
        {
            var velocity = force / _rb.mass * Time.fixedDeltaTime;
            var flightDuration = step * velocity.y / Physics.gravity.y;
            var stepTime = flightDuration / lineSegmentCount;

            _linePoints.Clear();
            _linePoints.Add(origin.position);

            for (var i = 1; i < lineSegmentCount; i++)
            {
                var stepTimePassed = stepTime * i;
                var movement = new Vector3(velocity.x * stepTimePassed,
                    velocity.y * stepTimePassed - 0.5f * Physics.gravity.y * stepTimePassed * stepTimePassed,
                    velocity.z * stepTimePassed);

                var newPosition = -movement + origin.position;
                var direction = newPosition - _linePoints[i - 1];

                if (Physics.Raycast(_linePoints[i - 1], direction, out var hit, direction.magnitude, groundLayerMask))
                {
                    _linePoints.Add(hit.point);
                    cursor.SetActive(true);
                    cursor.transform.position = new Vector3(hit.point.x, hit.point.y + 0.5f, hit.point.z);
                    break;
                }

                _linePoints.Add(newPosition);
                cursor.SetActive(false);
            }

            _line.positionCount = _linePoints.Count;
            _line.SetPositions(_linePoints.ToArray());
        }

        private void DestroyTrajectory()
        {
            _line.positionCount = 0;
            cursor.SetActive(false);
            _isShooting = false;
        }
    }
}