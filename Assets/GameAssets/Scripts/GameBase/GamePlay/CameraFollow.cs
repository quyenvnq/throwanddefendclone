﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class CameraFollow : BaseSingleton<CameraFollow>
    {
        [SerializeField] private ParticleSystem confettiFx;

        public void EnableConfetti()
        {
            if (confettiFx == null)
            {
                return;
            }

            confettiFx.SetActive();
        }
    }
}