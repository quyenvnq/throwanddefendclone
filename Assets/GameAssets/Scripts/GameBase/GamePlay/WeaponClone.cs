using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class WeaponClone : MonoBehaviour
    {
        public Image imgProgressBar;
        public Button btnWeapon;
        
        [SerializeField] private Image imgIcon;
        [SerializeField] private GameObject goPrefab;
        [SerializeField] private WeaponType weaponType;
        
        public void SetSpriteIcon(Sprite icon)
        {
            imgIcon.sprite = icon;
        }

        public void SetWeapon(GameObject weapon)
        {
            goPrefab = weapon;
        }

        public void SetWeaponType(WeaponType type)
        {
            weaponType = type;
        }
    }
}
