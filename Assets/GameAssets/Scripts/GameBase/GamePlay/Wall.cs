﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Wall : BaseCharacter
    {
        [SerializeField] private List<Transform> wallTargets = new List<Transform>();
        [SerializeField] private Transform road;

        #region Singleton

        public static Wall Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
            }

            stat.isFalling = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Instance == this)
            {
                Instance = null;
            }
        }

        #endregion

        public override void BeAttacked(float dmg)
        {
            base.BeAttacked(dmg);
            uiGameStart.UpdateHealthBar(CurrentFillAmount());
        }

        public override void Death()
        {
            stat.ChangeCharacterState(CharacterState.Death);
            GameManager.Instance.OnLose();
        }

        public float CurrentFillAmount()
        {
            return stat.hp / stat.maxHp;
        }

        public Transform GetWallTarget()
        {
            return wallTargets[Random.Range(0, wallTargets.Count - 1)];
        }
    }
}