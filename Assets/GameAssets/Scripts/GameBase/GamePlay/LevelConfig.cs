﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Serializable;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class LevelConfig : BaseSingleton<LevelConfig>
    {
        [SerializeField] private List<Weapon> weaponUnlock = new List<Weapon>();
        [SerializeField] private Transform weaponParent;
        [SerializeField] private GameObject weaponPrefab;

        public IEnumerable<Weapon> GetSubWeapons()
        {
            var level = UserDataManager.Instance.userDataSave.level;
            var subWeapons = new List<Weapon>();

            foreach (var x in weaponUnlock)
            {
                if (level < x.levelUnlock)
                {
                    continue;
                }

                var clone = Instantiate(weaponPrefab, weaponParent).GetComponent<WeaponClone>();
                
                clone.SetSpriteIcon(x.imgIcon);
                clone.SetWeapon(x.prefab);
                clone.SetWeaponType(x.weaponType);

                x.btnWeapon = clone.btnWeapon;
                x.imgProgressBar = clone.imgProgressBar;

                subWeapons.Add(x);
                clone.gameObject.SetActive(false);
            }

            return subWeapons;
        }
    }
}