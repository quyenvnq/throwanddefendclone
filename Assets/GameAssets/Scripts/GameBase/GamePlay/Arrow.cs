﻿using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Arrow : MonoBehaviour
    {
        [SerializeField] private GameObject bloodFx;
        [SerializeField] private GameObject trail;

        [SerializeField] private int minAmount = 3;
        [SerializeField] private int maxAmount = 7;
        [SerializeField] private float radiusAOE = 2f;

        [SerializeField] private float autoDestroyTime = 5f;
        [SerializeField] private float depth = 0.3f;
        [SerializeField] private float destroyFx = 1f;
        [SerializeField] private float damage = 5f;

        private Rigidbody _rb;
        private Collider _collider;

        private float _dmg;
        private bool _isHit;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
        }

        private void OnEnable()
        {
            transform.DORotate(Vector3.zero, 0.05f);
            SetAffectGravity(true);
            SetAtk(damage);
        }

        public void SetAtk(float atk)
        {
            _dmg = atk;
        }

        private void FixedUpdate()
        {
            if (_rb != null && !_rb.velocity.Equals(Vector3.zero))
            {
                transform.rotation = Quaternion.LookRotation(_rb.velocity);
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            switch (other.collider.tag)
            {
                case "Monster":
                    var c = other.collider.GetComponentInParent<Monster.Monster>();
                    AttackTarget(c, other.transform);
                    
                    if (!c.stat.IsAlive)
                    {
                        WaveConfig.Instance.DestroyMonster(c);
                    }

                    break;

                case "Ground":
                    ArrowStickIn(true);
                    SpawnerHelper.DestroySpawner(gameObject, autoDestroyTime);
                    break;

                case "Soldier":
                    AttackTarget(other.collider.GetComponentInParent<Soldier.Soldier>(), other.transform);
                    break;

                case "Wall":
                    if (_isHit)
                    {
                        return;
                    }

                    var wall = other.collider.GetComponent<Wall>();
                    wall.BeAttacked(_dmg);

                    SetAffectGravity(true);
                    Destroy(_rb);

                    transform.Translate(depth * Vector3.forward);
                    SpawnerHelper.DestroySpawner(gameObject, autoDestroyTime);
                    break;

                case "Water":
                    if (!_isHit)
                    {
                        trail.SetActive(false);
                        SpawnerHelper.DestroySpawner(gameObject);
                    }

                    break;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            switch (other.tag)
            {
                case "Monster":
                    var c = other.GetComponentInParent<Monster.Monster>();
                    AttackTarget(c, other.transform);

                    if (!c.stat.IsAlive)
                    {
                        WaveConfig.Instance.DestroyMonster(c);
                    }

                    break;

                case "Soldier":
                    AttackTarget(other.GetComponentInParent<Soldier.Soldier>(), other.transform);
                    break;

                case "Water":
                    if (!_isHit)
                    {
                        trail.SetActive(false);
                        SpawnerHelper.DestroySpawner(gameObject);
                    }

                    break;
            }
        }

        private void AttackTarget(BaseCharacter c, Transform parent, bool isDestroyRb = false)
        {
            if (c == null || _isHit)
            {
                return;
            }

            c.BeAttacked(_dmg);
            c.ResetVelocity();

            SpawnerHelper.CreateAndDestroy(parent.position, null, bloodFx, timeDestroy: destroyFx)
                .GetComponent<ParticleSystem>().Play();
            transform.SetParent(parent);
            ArrowStickIn(isDestroyRb);

            if (c.stat.IsAlive || !WaveConfig.Instance.CheckWin)
            {
                return;
            }

            GameManager.Instance.OnWin();
        }

        public void SingleShoot(Vector3 direction)
        {
            _isHit = false;
            trail.SetActive(true);

            if (_rb == null)
            {
                _rb = gameObject.AddComponent<Rigidbody>();
            }

            SetAffectGravity(false);
            _rb.AddForce(direction - _rb.velocity, ForceMode.Acceleration);
        }

        public void MultiShoot(Vector3 original, Vector3 direction)
        {
            var range = Random.Range(minAmount, maxAmount);
            SingleShoot(direction);
            
            for (var i = 0; i < range; i++)
            {
                var arrow = SpawnerHelper.CreateSpawner(original, null, gameObject).GetComponent<Arrow>();
                var vector = Vector3.ProjectOnPlane(Random.insideUnitSphere, original).normalized * radiusAOE;
                arrow.SingleShoot(direction + new Vector3(vector.x, 0f, vector.z));
            }
        }

        public void SetAffectGravity(bool active)
        {
            if (_rb == null)
            {
                return;
            }

            _rb.SetAffectGravity(_collider, active);

            if (!active)
            {
                _rb.ResetInertia();
            }
        }

        private void ArrowStickIn(bool isDestroyRb = false)
        {
            SetAffectGravity(true);
            trail.SetActive(false);
            transform.Translate(depth * transform.forward);
            _isHit = true;

            if (isDestroyRb)
            {
                return;
            }

            Destroy(_rb);
            transform.localPosition = Vector3.zero;
        }
    }
}