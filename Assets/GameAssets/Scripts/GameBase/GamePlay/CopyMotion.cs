﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class CopyMotion : MonoBehaviour
    {
        [SerializeField] private Transform targetLimb;

        private ConfigurableJoint _configurableJoint;
        private Quaternion _targetInitialRotation;

        private void Awake()
        {
            _configurableJoint = GetComponent<ConfigurableJoint>();
            _targetInitialRotation = targetLimb.localRotation;
        }

        private void FixedUpdate()
        {
            _configurableJoint.targetRotation = Quaternion.Inverse(targetLimb.localRotation) * _targetInitialRotation;
        }
    }
}