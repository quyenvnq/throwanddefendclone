﻿using GameAssets.Scripts.DlabComponents;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.State.Soldier;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay.Soldier
{
    public class Soldier : BaseCharacter
    {
        public AnimComponent AnimComponent1;
        [SerializeField] private GameObject trail;

        protected Monster.Monster target;
        private static readonly int Idle = Animator.StringToHash("Idle");
        private static readonly int Speed = Animator.StringToHash("Speed");

        protected bool isDontTarget;
        public bool IsKnockback { get; private set; }
        private static readonly int Up = Animator.StringToHash("StandUp");

        private Vector3 _road;

        protected override void Start()
        {
            base.Start();
            _road = GameObject.Find("Road").transform.position;
        }

        protected override void CheckDistance()
        {
            if (target == null)
            {
                return;
            }

            if (!target.stat.IsAlive)
            {
                DestroyMonster(target);
            }
        }

        protected void OnEnable()
        {
            if (!stat.IsAlive)
            {
                stat.Reset();
            }

            agent.enabled = false;
            SetAffectGravity(true);
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            
            if (isDontTarget)
            {
                GetNewTarget();
            }
        }

        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            SetTrail(false);
            switch (other.tag)
            {
                case "Ground":
                    if (!stat.isFalling)
                    {
                        return;
                    }

                    StandUp();
                    animator.OnComplete(
                        () =>
                        {
                            if (hip != null)
                            {
                                hip.angularXMotion = ConfigurableJointMotion.Limited;
                                hip.angularZMotion = ConfigurableJointMotion.Limited;
                            }
                            
                            stat.isFalling = false;
                            animator.SetBool(Idle, true);
                            ResetVelocity();

                            if (target != null && target.stat.IsAlive)
                            {
                                return;
                            }

                            GetNewTarget();
                        });

                    break;

                case "Monster":
                    if (!stat.isFalling)
                    {
                        return;
                    }

                    StandUp();
                    animator.OnComplete(
                        () =>
                        {
                            if (hip != null)
                            {
                                hip.angularXMotion = ConfigurableJointMotion.Limited;
                                hip.angularZMotion = ConfigurableJointMotion.Limited;
                            }
                            
                            stat.isFalling = false;
                            animator.SetBool(Idle, true);
                            target = other.GetComponentInParent<Monster.Monster>();

                            ResetVelocity();
                            ChangeAttack();
                        });

                    break;

                case "Water":
                    ChangeState();
                    Destroy(gameObject);
                    break;
            }
        }

        private void GetNewTarget()
        {
            target = WaveConfig.Instance.GetTarget(this);
            if (target == null || !target.stat.IsAlive)
            {
                isDontTarget = true;
                stat.ChangeCharacterState(General.CharacterState.Idle);
                ChangeAnimationState(0);

                if (WaveConfig.Instance.CheckWin)
                {
                    GameManager.Instance.OnWin();
                }
            }
            else
            {
                ChangeState(new SoldierMoveState(target));
            }
        }

        private void StandUp()
        {
            if (!stat.IsAlive)
            {
                return;
            }

            if (animatorController != null)
            {
                animator.runtimeAnimatorController = animatorController;
            }

            animator.SetFloat(Speed, stat.speedAtk);
            agent.speed = stat.moveSpeed;
            agent.enabled = true;

            ResetVelocity();
            agent.StopAgent();

            SetAffectGravity(false);
            GameManager.Instance.AddBaseCharacter(this);
        }

        public virtual void ChangeAttack()
        {
            isDontTarget = !(target != null && target.stat.IsAlive);
            if (!isDontTarget)
            {
                ChangeState(new SoldierAttackState(target));
            }
        }

        public void ChangeNewTarget()
        {
            ChangeState();
            agent.StopAgent();

            ResetVelocity();
            GetNewTarget();
        }

        public void SetTrail(bool active)
        {
            trail.SetActive(active);
        }

        public void Knockback(Vector3 origin)
        {
            if (IsKnockback)
            {
                return;
            }

            IsKnockback = true;

            ChangeState();
            ResetVelocity();
            agent.StopAgent();

            var dir = transform.position;
            var posEnd = dir + (dir - origin).normalized * Random.Range(10f, 15f);

            var limitX = Mathf.Clamp(posEnd.x, _road.x - _road.x / 2f - 10f, _road.x + _road.x / 2f - 10f);
            var limitY = Mathf.Clamp(posEnd.y, posEnd.y, posEnd.y + 5f);

            AnimComponent1.DoJump(new Vector3(limitX, limitY, posEnd.z), 15f, () =>
            {
                animator.SetTrigger(Up);
                animator.OnComplete(() =>
                {
                    IsKnockback = false;
                    if (target != null && target.stat.IsAlive)
                    {
                        ChangeState(new SoldierMoveState(target));
                    }
                    else
                    {
                        ChangeNewTarget();
                    }
                });
            });
        }

        public override void AttackTarget(Monster.Monster m)
        {
            if (m == null || !m.stat.IsAlive)
            {
                ChangeNewTarget();
                return;
            }

            target = m;
            LookTarget();

            m.Knockback(transform.position);
            m.BeAttacked(stat.atk);

            if (m != null && m.stat.IsAlive)
            {
                animator.ResetNormalizedTime("Slash");
            }

            DestroyMonster(m);
        }

        public void DestroyMonster(Monster.Monster m)
        {
            if (m != null && m.stat.IsAlive)
            {
                ChangeNewTarget();
                return;
            }

            if (!WaveConfig.Instance.CheckWin)
            {
                WaveConfig.Instance.DestroyMonster(m);
                ChangeNewTarget();
            }
            else
            {
                WaveConfig.Instance.DestroyMonster(m);
                GameManager.Instance.OnWin();
            }
        }

        protected void LookTarget()
        {
            if (transform == null || target == null)
            {
                return;
            }

            var lookPos = target.transform.position;
            lookPos.y = transform.position.y;
            transform.LookAt(lookPos);
        }

        public void SingleShoot(Vector3 direction, float thrustForce, float radius)
        {
            var randPos = Random.insideUnitCircle * radius;
            SetTrail(true);
            AnimComponent1.DoJump(direction + new Vector3(randPos.x, 0, randPos.y) * Random.Range(1, 1.8f),
                thrustForce * 3f,
                () =>
                {
                    foreach (var x in rbs)
                    {
                        x.isKinematic = false;
                    }
                });
        }
    }
}