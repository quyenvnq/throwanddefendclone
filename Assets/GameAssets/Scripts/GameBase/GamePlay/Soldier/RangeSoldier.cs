﻿using DG.Tweening;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.State.Soldier;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay.Soldier
{
    public class RangeSoldier : Soldier
    {
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;

        private Vector3 _force;

        public override void ChangeAttack()
        {
            isDontTarget = !(target != null && target.stat.IsAlive);
            if (!isDontTarget)
            {
                ChangeState(new RangeSoldierAttackState(target));
            }
        }

        public override void AttackTarget(Monster.Monster m)
        {
            if (m != null && m.stat.IsAlive)
            {
                target = m;
                var position = m.transform.position;
                LookTarget();

                var exitPos = exitPoint.position;
                var bullet = SpawnerHelper.CreateSpawner(exitPos, exitPoint, bulletPrefab);
                _force = position - exitPos;

                if (bullet.TryGetComponent<Arrow>(out var arrow))
                {
                    arrow.transform.SetParent(null);
                    arrow.transform.DORotate(new Vector3(180f, 20f, 0f), 0.05f);

                    arrow.SetAtk(stat.atk);
                    arrow.SetAffectGravity(false);
                    arrow.SingleShoot(new Vector3(_force.x, -_force.y, _force.z) * stat.thrustForce);
                }
                else if (bullet.TryGetComponent<Stone>(out var stone))
                {
                    stone.transform.SetParent(null);
                    stone.transform.DORotate(new Vector3(180f, 20f, 0f), 0.05f);

                    stone.SetAtk(stat.atk);
                    stone.SetAffectGravity(false);
                    stone.SingleShoot(new Vector3(_force.x, -_force.y, _force.z) * stat.thrustForce);
                }
            }
            else
            {
                DestroyMonster(m);
            }
        }
    }
}