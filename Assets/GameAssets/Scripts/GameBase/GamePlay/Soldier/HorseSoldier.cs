using DG.Tweening;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.State.Soldier;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay.Soldier
{
    public class HorseSoldier : Soldier
    {
        [SerializeField] private Animator animatorHorseman;
        private static readonly int State1 = Animator.StringToHash("State");
        private static readonly int Speed = Animator.StringToHash("Speed");

        public Animator AnimatorHorseman => animatorHorseman;

        protected override void Awake()
        {
            base.Awake();
            animatorHorseman.SetFloat(Speed, stat.speedAtk);
        }

        public override void Death()
        {
            base.Death();
            animatorHorseman.SetInteger(State1, 3);
        }
        
        public void Move()
        {
            animatorHorseman.SetInteger(State1, 1);
        }

        public override void ChangeAttack()
        {
            if (target != null && target.stat.IsAlive)
            {
                isDontTarget = false;
                animatorHorseman.SetInteger(State1, 2);

                ChangeAnimationState(0);
                ChangeState(new HorseSoldierAttackState(target));
            }
            else
            {
                isDontTarget = true;
            }
        }

        public override void AttackTarget(Monster.Monster m)
        {
            if (m == null || !m.stat.IsAlive)
            {
                ChangeNewTarget();
                return;
            }

            target = m;
            LookTarget();
            
            m.Knockback(transform.position);
            m.BeAttacked(stat.atk);
            
            if (m != null && m.stat.IsAlive)
            {
                animatorHorseman.ResetNormalizedTime("Slash");
            }

            DestroyMonster(m);
        }
    }
}