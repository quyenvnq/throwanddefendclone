﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Serializable;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class WaveConfig : BaseSingleton<WaveConfig>
    {
        [SerializeField] private List<WaveMonster> waveMonsters = new List<WaveMonster>();
        [SerializeField] private BoxCollider waveSpawnerArea;

        [Header("Wave")] [SerializeField] private int wave = 2;

        [Header("Wave bonus")] [SerializeField]
        private int waveBonus = 1;

        [SerializeField] private int maxWave = 100;
        [SerializeField] private int levelToAddWave = 2;

        [Header("Amount spawner")] [SerializeField]
        private int minAmountSpawner = 2;

        [SerializeField] private int maxAmountSpawner = 4;
        [SerializeField] private int maxSpawner = 10;

        [Header("Amount spawner bonus")] [SerializeField]
        private int levelToAddAmount = 2;

        [SerializeField] private int amountSpawnerToAddAtLevel = 2;

        [Header("Wave type")] [SerializeField] private bool nextWaveIfPrevWaveComplete;

        [Header("Wave Time")] [HideIf(nameof(nextWaveIfPrevWaveComplete))] [SerializeField]
        private float waveMinTime = 2f;

        [HideIf(nameof(nextWaveIfPrevWaveComplete))] [SerializeField]
        private float waveMaxTime = 4f;

        [HideIf(nameof(nextWaveIfPrevWaveComplete))] [Header("Wave time bonus")] [SerializeField]
        private int levelToDecreaseTime = 1;

        [HideIf(nameof(nextWaveIfPrevWaveComplete))] [SerializeField]
        private float slowTime = 10f;

        [HideIf(nameof(nextWaveIfPrevWaveComplete))] [SerializeField]
        private float timeToDecreaseAtLevel = 0.5f;

        [HideIf(nameof(nextWaveIfPrevWaveComplete))] [SerializeField]
        private float waveMinimumTime = 1f;

        private readonly List<Monster.Monster> _monsters = new List<Monster.Monster>();

        public int MonsterCoinWin { get; private set; }

        private int _wave;
        private float _waveSpawnerTime;
        private float _waveTime;

        private void Start()
        {
            var bonus = wave + waveBonus * UserDataManager.Instance.userDataSave.level / levelToAddWave;
            _wave = bonus >= maxWave ? maxWave : bonus;
            _waveSpawnerTime = 0f;
        }

        private void GetWaveSpawnerTime()
        {
            var level = UserDataManager.Instance.userDataSave.level;
            var decTime = timeToDecreaseAtLevel * level / levelToDecreaseTime;
            var maxTime = waveMaxTime - decTime < waveMinimumTime ? waveMinimumTime : waveMaxTime - decTime;
            _waveSpawnerTime = Random.Range(waveMinTime, maxTime);
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            _waveTime += Time.deltaTime / slowTime;
            if (_wave >= 1 && (nextWaveIfPrevWaveComplete ? AllMonsterDie : _waveTime >= _waveSpawnerTime))
            {
                SpawnerMonster();
            }
        }

        public void IncreaseMonsterCoin(int coin)
        {
            MonsterCoinWin += coin;
        }

        private void SpawnerMonster()
        {
            var level = UserDataManager.Instance.userDataSave.level + 1;
            var gos = (from x in waveMonsters where level >= x.levelUnlock select x.monsterPrefab).ToList();

            var maxAmount = maxAmountSpawner + level / levelToAddAmount * amountSpawnerToAddAtLevel;
            var amount = Random.Range(minAmountSpawner, maxAmount > maxSpawner ? maxSpawner : maxAmount);
            var clones = waveSpawnerArea.SpawnerInsideCollider(gos, amount);

            foreach (var x in clones)
            {
                var m = x.GetComponentInChildren<Monster.Monster>();
                _monsters.Add(m);
                GameManager.Instance.AddBaseCharacter(m);
            }

            _wave--;
            _waveTime = 0f;
            GetWaveSpawnerTime();
        }

        public void DestroyMonster(Monster.Monster m)
        {
            if (m == null)
            {
                return;
            }

            _monsters.Remove(m);

            var arrow = m.GetComponentsInChildren<Arrow>();
            foreach (var x in arrow)
            {
                x.transform.SetParent(null);
                SpawnerHelper.DestroySpawner(x.gameObject);
            }

            SpawnerHelper.DestroySpawner(m.gameObject, m.stat.timeDestroy);
        }

        public Monster.Monster GetTarget(Soldier.Soldier s)
        {
            if (_monsters.Count < 1)
            {
                return null;
            }

            var target = _monsters[0];

            if (target == null || s == null)
            {
                return null;
            }

            var min = Vector3.Distance(target.transform.position, s.transform.position);

            foreach (var x in _monsters)
            {
                if (x == null || !x.stat.IsAlive)
                {
                    continue;
                }

                var dis = Vector3.Distance(x.transform.position, s.transform.position);
                if (!(min >= dis))
                {
                    continue;
                }

                min = dis;
                target = x;
            }

            return target;
        }

        public bool CheckWin => _wave < 1 && AllMonsterDie;

        private bool AllMonsterDie => _monsters.All(x => x == null || !x.stat.IsAlive);
    }
}