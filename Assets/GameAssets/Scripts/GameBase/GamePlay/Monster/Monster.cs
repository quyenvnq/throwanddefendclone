﻿using GameAssets.Scripts.DlabComponents;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.State.Monster;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameAssets.Scripts.GameBase.GamePlay.Monster
{
    public class Monster : BaseCharacter
    {
        [FormerlySerializedAs("AnimComponent1")] [SerializeField]
        protected AnimComponent animComponent1;

        [SerializeField] private LayerMask guardianLayerMask;

        [SerializeField] private int levelToAddUpgrade = 1;
        [SerializeField] private int hpAddAtLevel = 1;
        [SerializeField] private int atkAddAtLevel = 1;

        [field: SerializeField] public int MonsterCoin { get; } = 1;

        private static readonly int Speed = Animator.StringToHash("Speed");
        private readonly Collider[] _overlapColliders = new Collider[10];
        protected BaseCharacter target;

        private Wall _wall;
        private Vector3 _road;

        private bool IsKnockBack { get; set; }

        protected override void Awake()
        {
            base.Awake();
            _wall = Wall.Instance;
            target = _wall;

            _road = GameObject.Find("Road").transform.position;
            agent.speed = stat.moveSpeed;

            stat.hp += UserDataManager.Instance.userDataSave.level / levelToAddUpgrade * hpAddAtLevel;
            stat.atk += UserDataManager.Instance.userDataSave.level / levelToAddUpgrade * atkAddAtLevel;

            animator.SetFloat(Speed, stat.speedAtk);
            ChangeState(new MonsterMoveState(_wall.GetWallTarget()));
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            
            if (target != null && !(target is Wall))
            {
                return;
            }

            var size = Physics.OverlapSphereNonAlloc(transform.position, stat.rangeDetectAtk, _overlapColliders,
                guardianLayerMask);

            for (var i = 0; i < size; i++)
            {
                var x = _overlapColliders[i];
                switch (x.tag)
                {
                    case "Soldier":
                        ChangeAttack(x.GetComponentInParent<Soldier.Soldier>());
                        break;

                    case "Wall":
                        ChangeAttack(x.GetComponent<Wall>());
                        break;
                }
            }
        }

        private void ChangeAttack(BaseCharacter bc)
        {
            if (bc == null)
            {
                return;
            }

            if (!bc.stat.IsAlive || bc.stat.isFalling ||
                ((bc as Soldier.Soldier)?.IsKnockback ?? false))
            {
                if (bc is Wall && !bc.stat.IsAlive)
                {
                    GameManager.Instance.StopBaseCharacter();
                }

                return;
            }

            if (target.Equals(bc))
            {
                return;
            }

            target = bc;
            ChangeState();
            ChangeState(new MonsterMoveState(target.transform));
        }

        public override void AttackTarget()
        {
            if (target == null)
            {
                DestroySoldier(true);
                return;
            }

            LookTarget();
            target.BeAttacked(stat.atk);
            DestroySoldier(true);
        }

        protected void LookTarget()
        {
            if (transform == null || target == null)
            {
                return;
            }
        
            var lookPos = target.transform.position;
            lookPos.y = transform.position.y;
            transform.LookAt(lookPos);
        }

        public virtual void ChangeAttackDistance()
        {
            LookTarget();
            ChangeState(new MonsterAttackState());
        }

        public void Knockback(Vector3 origin)
        {
            if (IsKnockBack)
            {
                return;
            }

            IsKnockBack = true;
            ChangeState();
            ResetVelocity();
            agent.StopAgent();

            var dir = transform.position;
            var posEnd = dir + (dir - origin).normalized * Random.Range(10f, 15f);

            var limitX = Mathf.Clamp(posEnd.x, _road.x - _road.x / 2f - 10f, _road.x + _road.x / 2f - 10f);
            var limitY = Mathf.Clamp(posEnd.y, posEnd.y, posEnd.y + 5f);

            animComponent1.DoJump(new Vector3(limitX, limitY, posEnd.z), 15f, () =>
            {
                IsKnockBack = false;
                
                if (target != null && target.stat.IsAlive)
                {
                    ChangeState(new MonsterMoveState(target.transform));
                }
                else
                {
                    ChangeWallTarget();
                }
            }, true, this is HorseMonster ? 1f : 0.25f);
        }

        public void DestroySoldier(bool reset = false)
        {
            if (target != null && target is Soldier.Soldier s)
            {
                if (!s.stat.IsAlive)
                {
                    target = null;
                    Destroy(s.gameObject, s.stat.timeDestroy);
                    var arrow = s.GetComponentsInChildren<Arrow>();
                    
                    foreach (var x in arrow)
                    {
                        if (x == null)
                        {
                            continue;
                        }

                        x.transform.SetParent(null);
                        SpawnerHelper.DestroySpawner(x.gameObject);
                    }
                }
                else
                {
                    s.Knockback(transform.position);
                }
            }

            if (reset && target != null)
            {
                animator.ResetNormalizedTime("Slash");
            }

            if (target != null)
            {
                return;
            }

            ChangeWallTarget();
        }

        public void ChangeWallTarget()
        {
            target = _wall;
            
            if (target.stat.IsAlive)
            {
                ChangeState(new MonsterMoveState(_wall.GetWallTarget()));
            }
            else
            {
                ChangeState();
                ResetVelocity();
                agent.StopAgent();
            }
        }

        public override void BeAttacked(float dmg)
        {
            if (!stat.IsAlive)
            {
                return;
            }

            stat.AddHp(-dmg);

            if (!stat.OutOfHp())
            {
                return;
            }

            Death();
            WaveConfig.Instance.IncreaseMonsterCoin(MonsterCoin);
        }
    }
}