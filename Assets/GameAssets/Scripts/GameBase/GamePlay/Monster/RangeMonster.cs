﻿using DG.Tweening;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.State.Monster;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay.Monster
{
    public class RangeMonster : Monster
    {
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;

        private Vector3 _force;

        protected override void Awake()
        {
            base.Awake();
            SetAffectGravity(true);
        }

        public override void ChangeAttackDistance()
        {
            LookTarget();
            ChangeState(new RangeMonsterAttackState());
        }

        public override void AttackTarget()
        {
            rbs[0].ResetInertia();
            
            if (target != null && target.stat.IsAlive)
            {
                var position = target.transform.position;
                LookTarget();

                var exitPos = exitPoint.position;
                var bullet = SpawnerHelper.CreateSpawner(exitPos, exitPoint, bulletPrefab);
                _force = position - exitPos;

                if (bullet.TryGetComponent<Arrow>(out var arrow))
                {
                    arrow.transform.SetParent(null);
                    arrow.transform.DORotate(new Vector3(180f, 20f, 0f), 0.1f);

                    arrow.SetAtk(stat.atk);
                    arrow.SingleShoot(new Vector3(_force.x, -_force.y, _force.z) * stat.thrustForce);
                }
                else if (bullet.TryGetComponent<Stone>(out var stone))
                {
                    stone.SetAtk(stat.atk);
                    stone.transform.DORotate(new Vector3(180f, 20f, 0f), 0.1f);
                    stone.SingleShoot(new Vector3(_force.x, -_force.y, _force.z) * stat.thrustForce);
                }
            }
            else
            {
                DestroySoldier();
            }
        }
    }
}