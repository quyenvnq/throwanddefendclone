using GameAssets.Scripts.State.Monster;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay.Monster
{
    public class HorseMonster : Monster
    {
        [SerializeField] private Animator animatorHorseman;
        private static readonly int State1 = Animator.StringToHash("State");
        private static readonly int Speed1 = Animator.StringToHash("Speed");

        public Animator AnimatorHorseman => animatorHorseman;

        protected override void Awake()
        {
            base.Awake();
            animatorHorseman.SetFloat(Speed1, stat.speedAtk);
        }

        public void Move()
        {
            animatorHorseman.SetInteger(State1, 1);
        }
        
        public override void ChangeAttackDistance()
        {
            animatorHorseman.SetInteger(State1, 2);
            ChangeAnimationState(0);
            
            LookTarget();
            ChangeState(new HorseMonsterAttackState());
        }

        public override void Death()
        {
            base.Death();
            animatorHorseman.SetInteger(State1, 3);
        }
    }
}
