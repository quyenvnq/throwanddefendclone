using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    [RequireComponent(typeof(BaseStat))]
    public class Stone : MonoBehaviour
    {
        [SerializeField] private GameObject smokeFx;
        [SerializeField] private GameObject trail;

        [SerializeField] private Rigidbody rb;
        [SerializeField] private new SphereCollider collider;
        [SerializeField] private BaseStat stat;

        [SerializeField] private int minAmount = 3;
        [SerializeField] private int maxAmount = 7;
        [SerializeField] private float radiusAOE = 2f;

        [SerializeField] private float autoDestroyTime = 7f;
        [SerializeField] private float destroyFx = 1f;

        public bool IsHit { get; private set; } = true;

        private float _dmg;

        private void Awake()
        {
            stat.atk += UserDataManager.Instance.userDataSave.upgrades[2].basePoint;
        }

        private void OnEnable()
        {
            transform.DORotate(Vector3.zero, 0.05f);
            SetAffectGravity(true);
            SetAtk(stat.atk);
        }

        public void SetAtk(float atk)
        {
            _dmg = atk;
        }

        private void DestroyObject()
        {
            trail.SetActive(false);
            SpawnerHelper.CreateAndDestroy(transform.position, null, smokeFx, timeDestroy: destroyFx)
                .GetComponent<ParticleSystem>().Play();
            SpawnerHelper.DestroySpawner(gameObject);
        }

        private void OnCollisionEnter(Collision other)
        {
            switch (other.collider.tag)
            {
                case "Monster":
                case "GiantMonster":
                    var monster = other.collider.GetComponentInParent<Monster.Monster>();
                    
                    if (monster != null)
                    {
                        rb.ResetInertia();
                        monster.ResetVelocity();

                        monster.BeAttacked(_dmg);
                        DestroyObject();
                        IsHit = false;

                        if (!monster.stat.IsAlive)
                        {
                            WaveConfig.Instance.DestroyMonster(monster);
                            if (WaveConfig.Instance.CheckWin)
                            {
                                GameManager.Instance.OnWin();
                            }
                        }
                    }

                    break;

                case "Soldier":
                    var soldier = other.collider.GetComponentInParent<Soldier.Soldier>();

                    if (soldier != null)
                    {
                        rb.ResetInertia();
                        soldier.ResetVelocity();
                        soldier.BeAttacked(_dmg);

                        DestroyObject();
                        IsHit = false;

                        if (!soldier.stat.IsAlive)
                        {
                            Destroy(soldier, soldier.stat.timeDestroy);
                        }
                    }

                    break;

                case "Wall":
                    var wall = other.collider.GetComponent<Wall>();
                    wall.BeAttacked(_dmg);

                    DestroyObject();
                    IsHit = false;
                    break;

                case "Water":
                    DestroyObject();
                    IsHit = false;
                    break;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            switch (other.tag)
            {
                case "Monster":
                    var monster = other.GetComponentInParent<Monster.Monster>();

                    if (monster != null)
                    {
                        rb.ResetInertia();
                        monster.ResetVelocity();
                        monster.BeAttacked(_dmg);

                        DestroyObject();
                        IsHit = false;

                        if (!monster.stat.IsAlive)
                        {
                            WaveConfig.Instance.DestroyMonster(monster);

                            if (WaveConfig.Instance.CheckWin)
                            {
                                GameManager.Instance.OnWin();
                            }
                        }
                    }

                    break;
            }
        }

        public void SingleShoot(Vector3 direction)
        {
            SetAffectGravity(false);
            transform.SetParent(null);

            trail.SetActive(true);
            rb.AddForce(direction - rb.velocity, ForceMode.Acceleration);
        }

        public void MultiShoot(Vector3 original, Vector3 direction)
        {
            var range = Random.Range(minAmount, maxAmount);
            SingleShoot(direction);

            for (var i = 0; i < range; i++)
            {
                var stone = SpawnerHelper.CreateSpawner(original, null, gameObject).GetComponent<Stone>();
                var vector = Vector3.ProjectOnPlane(Random.insideUnitSphere, original).normalized * radiusAOE;
                stone.SingleShoot(direction + new Vector3(vector.x, 0f, vector.z));
            }
        }

        public void SetAffectGravity(bool active)
        {
            rb.ResetInertia();
            rb.SetAffectGravity(collider, active);
        }
    }
}