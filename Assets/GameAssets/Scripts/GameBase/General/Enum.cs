﻿namespace GameAssets.Scripts.GameBase.General
{
    public enum SoldierType
    {
        MeleeCombat,
        Range
    }
    
    public enum HapticType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public enum CharacterState
    {
        Idle = 0,
        Move = 1,
        Attack = 2,
        Death = 3,
        Respawn = 4
    }

    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        PreparePlay,
        Playing,
        Lose,
        Win
    }

    public enum UIType
    {
        None,
        UILobby,
        UILoading,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIStore,
        UIShop
    }

    public enum SelectedMode
    {
        Ragdoll,
        Collider,
        Joints,
        CenterOfMass
    }
    
    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns,
        FixedBoth
    }

    public enum Alignment
    {
        Horizontal,
        Vertical
    }
}