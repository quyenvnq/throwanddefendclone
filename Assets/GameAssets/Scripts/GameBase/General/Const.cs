﻿namespace GameAssets.Scripts.GameBase.General
{
    public static class Const
    {
        public const string SAVE_DATA = "UserSaveData";
        public const string PASSWORD_HASH = "PASSWORD_HASHQUSDROKNEW";
        public const string SALT_KEY = "SALT_KEYQUSDROKNEW";
        public const string VI_KEY = "VI_KEYQUSDROKNEW";
        
        public const double MIN_NORMAL = 2.2250738585072014E-308d;
        
        public const long LIGHT_DURATION = 20;
        public const long MEDIUM_DURATION = 40;
        public const long HEAVY_DURATION = 80;
        
        public const int MIN_INTERVAL = 10;
        public const int INTERVAL = 20;
        public const int MAX_INTERVAL = 30;
        
        public const int LIGHT_AMPLITUDE = 40;
        public const int MEDIUM_AMPLITUDE = 120;
        public const int HEAVY_AMPLITUDE = 255;
    }
}