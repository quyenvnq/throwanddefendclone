﻿using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameBase.Manager;

namespace GameAssets.Scripts.GameBase.Base
{
    public abstract class BaseState : IState
    {
        public BaseCharacter Character { get; set; }

        private bool _isCompleted;
        private bool _isFrameCompleted;

        public void StartState()
        {
            if (!Character.stat.IsAlive || !GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            /*            Debug.Log($"{Character.name} START BS {Character.currentState}");*/
            InnerStartState();
        }

        protected virtual void InnerStartState()
        {
        }

        public void UpdateState()
        {
            if (_isCompleted || _isFrameCompleted || !Character.stat.IsAlive ||
                !GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            InnerUpdateState();
        }

        protected virtual void InnerUpdateState()
        {
        }

        public virtual void ExitState()
        {
        }

        protected void SetIsComplete(bool completed)
        {
            _isCompleted = completed;
        }

        protected void SetIsFrameCompleted(bool frameCompleted)
        {
            _isFrameCompleted = frameCompleted;
        }

        protected void OnCompleted()
        {
            if (!_isCompleted || !Character.stat.IsAlive ||
                !GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            SetIsComplete(false);
            InnerOnComplete();
        }

        protected virtual void InnerOnComplete()
        {
        }

        // ReSharper disable Unity.PerformanceAnalysis
        protected void OnFrameCompleted()
        {
            if (!_isFrameCompleted || !Character.stat.IsAlive ||
                !GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            SetIsFrameCompleted(false);
            InnerOnFrameCompleted();
        }

        protected virtual void InnerOnFrameCompleted()
        {
        }
    }
}