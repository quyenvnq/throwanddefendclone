﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.GamePlay.Monster;
using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Manager.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

namespace GameAssets.Scripts.GameBase.Base
{
    [RequireComponent(typeof(BaseStat))]
    public abstract class BaseCharacter : MonoBehaviour
    {
        public List<Rigidbody> rbs = new List<Rigidbody>();
        public Animator animator;
        public ConfigurableJoint hip;
        
        [ReadOnly] public List<Collider> colliders = new List<Collider>();
        [ReadOnly] public NavMeshAgent agent;
        [ReadOnly] public BaseStat stat;

        [SerializeField] private List<Renderer> partDie = new List<Renderer>();
        [SerializeField] private Material deathMaterial;
        [SerializeField] protected RuntimeAnimatorController animatorController;

        protected UIGameStart uiGameStart;
        protected UIPlay uiPlay;
        public IState currentState;

        private int _indexAnimation;

        private static readonly int State = Animator.StringToHash("State");

        #region Virtual Monobehaviour

        [Button]
        public void GetRender()
        {
            partDie.Clear();
            partDie.AddRange(GetComponentsInChildren<Renderer>());
        }
        protected virtual void Awake()
        {
            colliders.Clear();
            colliders.AddRange(GetComponentsInChildren<Collider>());
            agent = GetComponent<NavMeshAgent>();

            uiGameStart = UIManager.Instance.GetUI<UIGameStart>();
            uiPlay = UIManager.Instance.GetUI<UIPlay>();
            stat = GetComponent<BaseStat>();

            if (UserDataManager.Instance.userDataSave.upgrades.Count < 1)
            {
                UserDataManager.Instance.SetUpgrade(uiPlay.upgrades.ToArray());
            }

            if (this is Monster)
            {
                return;
            }

            stat.hp += UserDataManager.Instance.userDataSave.upgrades[1].basePoint;
            stat.atk += UserDataManager.Instance.userDataSave.upgrades[2].basePoint;
        }

        protected virtual void Start()
        {
        }

        private void Update()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            if (stat.IsAttack)
            {
                CheckDistance();
            }

            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerLateUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
        }

        protected virtual void OnDestroy()
        {
        }

        #endregion

        public virtual void BeAttacked(float dmg)
        {
            if (!stat.IsAlive)
            {
                return;
            }

            stat.AddHp(-dmg);
            
            if (stat.OutOfHp())
            {
                Death();
            }
        }

        public virtual void Death()
        {
            ChangeState();
            ChangeAnimationState(3);
            stat.ChangeCharacterState(CharacterState.Death);

            if (hip != null)
            {
                hip.angularXMotion = ConfigurableJointMotion.Free;
                hip.angularZMotion = ConfigurableJointMotion.Free;
            }
            
            foreach (var x in partDie)
            {
                x.material = deathMaterial;
            }

            if (agent != null)
            {
                agent.enabled = false;
            }

            if (colliders.Count < 1)
            {
                colliders[0].isTrigger = true;
            }

            foreach (var x in rbs.Where(x => x != null))
            {
                x.isKinematic = false;
            }
        }

        public void ResetVelocity()
        {
            foreach (var x in rbs.Where(x => x != null))
            {
                x.ResetInertia();
            }
        }

        public void SetAffectGravity(bool active, bool isOnlyCollider = false)
        {
            for (var i = 0; i < rbs.Count; i++)
            {
                if (active)
                {
                    rbs[i].ResetInertia();
                }

                rbs[i].SetAffectGravity(colliders[i], active, isOnlyCollider);
            }
        }

        public void ChangeAnimationState(int index)
        {
            if (_indexAnimation.Equals(index))
            {
                return;
            }

            _indexAnimation = index;
            animator.SetInteger(State, index);
        }

        public void ChangeAnimatorLayer(string layerName, bool active = true)
        {
            for (var i = 0; i < animator.layerCount; i++)
            {
                animator.SetLayerWeight(i, 0);
            }

            animator.SetLayerWeight(animator.GetLayerIndex(layerName), active ? 1 : 0);
        }

        public void ChangeState(IState newState = null)
        {
            ResetVelocity();
            
            if ((currentState?.ToString() ?? "Null").Equals(newState?.ToString() ?? "Null"))
            {
                return;
            }

            currentState?.ExitState();
            currentState = newState;

            if (currentState == null)
            {
                return;
            }

            /*Debug.Log($"{name} STATE ===> {newState}");*/
            currentState.Character = this;
            currentState.StartState();
        }

        public virtual void AttackTarget()
        {
        }

        public virtual void AttackTarget(Monster m)
        {
        }

        protected virtual void CheckDistance()
        {
        }
    }
}