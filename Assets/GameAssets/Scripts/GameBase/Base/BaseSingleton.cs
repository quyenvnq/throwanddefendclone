﻿using GameAssets.Scripts.GameBase.General;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }
            private set => _instance = value;
        }

        [SerializeField] private bool isDontDestroy;

        #region Virtual MonoBehaviour

        protected virtual void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            InitAwake();
            
            if (!isDontDestroy)
            {
                return;
            }

            if (transform.parent != null)
            {
                transform.SetParent(null);
            }

            DontDestroyOnLoad(this);
        }

        protected virtual void InitAwake()
        {
            
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.OnWin();
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                GameManager.Instance.OnLose();
            }

            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            InnerFixedUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        protected virtual void OnDestroy()
        {
            if (Equals(Instance, this))
            {
                Instance = default;
            }
        }

        #endregion
    }
}