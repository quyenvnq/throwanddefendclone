using GameAssets.Scripts.GameBase.General;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameAssets.Scripts.GameBase.Base
{
    public sealed class BaseStat : MonoBehaviour
    {
        public SoldierType soldierType;
        public CharacterState characterState;

        public float hp;
        public float maxHp;
        public float atk;
        
        public float timeDestroy = 2f;
        public float thrustForce = 15f;
        
        public float moveSpeed;
        public float speedAtk;
        public float rangeAtk;
        public float rangeDetectAtk;
        
        public float frameAtk = 0.15f;
        [FormerlySerializedAs("isStandUp")] public bool isFalling = true;
        
        private bool IsMove { get; set; }
        public bool IsAttack { get; set; }
        public bool IsAlive { get; set; } = true;

        private void OnValidate()
        {
            maxHp = hp;
        }

        public void Reset()
        {
            hp = maxHp;
            ChangeCharacterState(CharacterState.Idle);
        }

        public void AddAtk(float a)
        {
            atk += a;
        }

        public void AddHp(float h)
        {
            hp += h;
        }

        public bool OutOfHp()
        {
            return hp <= 0;
        }
        
        public bool IsCharacterState(CharacterState state)
        {
            return characterState.Equals(state);
        }

        public void SetCharacterState(CharacterState state)
        {
            characterState = state;
        }
        
        public bool ChangeCharacterState(CharacterState state)
        {
            if (IsCharacterState(state))
            {
                return false;
            }

            SetCharacterState(state);

            /*Debug.Log($"{name} CS ===> {state}");*/
            switch (state)
            {
                case CharacterState.Idle:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;

                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;

                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;

                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;
                    break;
                
                case CharacterState.Respawn:
                    IsAlive = true;
                    break;
            }

            return true;
        }
    }
}