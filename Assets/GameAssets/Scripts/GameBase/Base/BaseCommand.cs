﻿using GameAssets.Scripts.GameBase.Interface;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseCommand : ICommand
    {
        public virtual void Execute()
        {
        }

        public virtual bool IsFinished()
        {
            return false;
        }
    }
}