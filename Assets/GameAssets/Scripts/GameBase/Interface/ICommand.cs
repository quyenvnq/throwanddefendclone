﻿namespace GameAssets.Scripts.GameBase.Interface
{
    public interface ICommand
    {
        void Execute();
        bool IsFinished();
    }
}
